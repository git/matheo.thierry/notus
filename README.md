[![Build Status](https://codefirst.iut.uca.fr/api/badges/matheo.thierry/notus/status.svg)](https://codefirst.iut.uca.fr/matheo.thierry/notus)  
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=notus_ThMo&metric=alert_status&token=f49680ff7f8cbaaa0872d836ad12346debbf5164)](https://codefirst.iut.uca.fr/sonar/dashboard?id=notus_ThMo)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=notus_ThMo&metric=coverage&token=f49680ff7f8cbaaa0872d836ad12346debbf5164)](https://codefirst.iut.uca.fr/sonar/dashboard?id=notus_ThMo)
[![Lines of Code](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=notus_ThMo&metric=ncloc&token=f49680ff7f8cbaaa0872d836ad12346debbf5164)](https://codefirst.iut.uca.fr/sonar/dashboard?id=notus_ThMo)

# SAE 2.01 - Développement d'une application

## Présentation 

Dans le cadre de la SAE, notre mission consiste à développer une application de prise de notes. Cette application sera un outil pratique pour les personnes qui cherchent à organiser leurs idées et leurs pensées.

Notre application sera facile à utiliser et intuitive, avec une interface simple et épurée qui permettra à l'utilisateur de s'orienter facilement. Elle permettra de partager les notes avec d'autres utilisateurs et de les gérer avec des tags. L’application sera personnalisable grâce à la possibilité de choisir parmi plusieurs thèmes et logos fournis. Les utilisateurs pourront également créer leurs propres thèmes.