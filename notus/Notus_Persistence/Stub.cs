﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;


namespace Notus_Persistance
{
    public class Stub : IManager
    {
        public void SaveDatabaseData(List<User> UserList)
        {
            throw new NotImplementedException();
        }

        public void SaveDefaultData(List<Theme> DefaultThemeList, List<Logo> DefaultLogoList)
        {
            throw new NotImplementedException() ;
        }

        //Loaders
        List<User> IManager.LoadDatabaseData()
        {
            List<User> userList = new List<User>();
            Note nselect;
            User uselect;

            // add some users
            userList.Add(new User("Nicolas", "leHeros@gmail.com", "FeurFeur"));
            userList.Add(new User("Benjamin", "labsent@gmail.com", "Moto2005"));
            userList.Add(new User("Liam", "liammonchanin@gmail.com", "Baguette"));
            userList.Add(new User("Brigitte", "Macroutte@gmail.com", "4949Trois"));

            // add some notes and tags to go faster

            for (int i = 0; i < userList.Count; i++)
            {
                userList[i].CreateNote($"Note {i}", "DefaultLogo.png");
                userList[i].CreateTag($"Tag {i}", "#5555FF");
            }

            // add note to user for sharing note test mixed with tag

            uselect = userList[0];
            nselect = uselect.NoteList[0];
            nselect.AddCollaborator(uselect, userList[1]);
            nselect.AddCollaborator(uselect, userList[2]);
            uselect.AddTagFromNoteList(nselect, uselect.TagList[0]);
            nselect.AddEditor(uselect, userList[2]);

            // add some default logos and themes
            foreach (User user in userList)
            {
                user.ChangePassword(user.Password);
            }

            return userList;
        }

        public List<Theme> LoadDefaultTheme()
        {
            List<Theme> DefaultThemeList = new List<Theme>();
            DefaultThemeList.Add(new("blacktheme", "#000000,#FF00FF,#OOFFOO".Split(',').ToList()));
            DefaultThemeList.Add(new("whitetheme", "#FFFFFF,#FF00FF,#OOFFOO".Split(',').ToList()));
            return DefaultThemeList;
        }
        public List<Logo> LoadDefaultLogo()
        {
            List<Logo> DefaultLogoList = new List<Logo>();
            DefaultLogoList.Add(new("default","DefaultLogo.png"));
            DefaultLogoList.Add(new("1", "Logo1.png"));
            DefaultLogoList.Add(new("2", "Logo2.png"));
            return DefaultLogoList;
        }

    }
}

