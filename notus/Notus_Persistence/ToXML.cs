﻿using Biblioteque_de_Class;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Diagnostics.CodeAnalysis;

namespace Notus_Persistance
{
    [ExcludeFromCodeCoverage]
    public class ToXML : IManager
    {
        // /../../../..
        private string DataFilePath { get; set; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NotusData");
        private const string DataFileName = "/Userdata.xml";
        private const string DefaultThemeName = "/defaultTheme.xml";
        private const string DefaultLogoName = "/defaultLogo.xml";
        private static readonly DataContractSerializer DatabaseXmlSerializer = new(typeof(Database), GetKnownTypes());

        public void SaveDatabaseData(List<User> UserList)
        {
            if (!Directory.Exists(DataFilePath))
            {
                Directory.CreateDirectory(DataFilePath);
            }
            XmlWriterSettings settings = new() { Indent = true };
            using TextWriter tw = File.CreateText(Path.Combine(DataFilePath + DataFileName));
            using XmlWriter writer = XmlWriter.Create(tw, settings);
            DatabaseXmlSerializer.WriteObject(writer, UserList);
        }

        public void SaveDefaultData(List<Theme> DefaultThemeList, List<Logo> DefaultLogoList)
        {
            if (!Directory.Exists(DataFilePath))
            { Directory.CreateDirectory(DataFilePath); }
            XmlWriterSettings settings = new() { Indent = true };
            using TextWriter tw = File.CreateText(Path.Combine(DataFilePath + DefaultThemeName));
            using XmlWriter writer = XmlWriter.Create(tw, settings);
            DatabaseXmlSerializer.WriteObject(writer, DefaultThemeList);

            using TextWriter tw2 = File.CreateText(Path.Combine(DataFilePath + DefaultLogoName));
            using XmlWriter writer2 = XmlWriter.Create(tw2, settings);
            DatabaseXmlSerializer.WriteObject(writer2, DefaultLogoList);
        }

        private static IEnumerable<Type> GetKnownTypes()
        {
            yield return typeof(User);
            yield return typeof(List<User>);
            yield return typeof(Theme);
            yield return typeof(List<Theme>);
            yield return typeof(Logo);
            yield return typeof(List<Logo>);
        }

        public List<User> LoadDatabaseData()
        {
            if (File.Exists(Path.Combine(DataFilePath + DataFileName)))
            {
                using (FileStream fileStream = File.OpenRead(Path.Combine(DataFilePath + DataFileName)))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not List<User> user
                        ? throw new FileException("Failed to load Database. The loaded object is null.")
                        : user;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

        public List<Theme> LoadDefaultTheme()
        {
            if (File.Exists(Path.Combine(DataFilePath + DefaultThemeName)))
            {
                using (FileStream fileStream = File.OpenRead(Path.Combine(DataFilePath + DefaultThemeName)))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not List<Theme> DefaultThemeList
                        ? throw new FileException("Failed to load Default Theme. The loaded object is null.")
                        : DefaultThemeList;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

        public List<Logo> LoadDefaultLogo()
        {
            if (File.Exists(Path.Combine(DataFilePath + DefaultLogoName)))
            {
                using (FileStream fileStream = File.OpenRead(Path.Combine(DataFilePath + DefaultLogoName)))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not List<Logo> DefaultLogoList
                        ? throw new FileException("Failed to load Default Logo. The loaded object is null.")
                        : DefaultLogoList;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

    }
}
