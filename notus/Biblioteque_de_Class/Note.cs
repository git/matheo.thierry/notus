﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;

namespace Biblioteque_de_Class
{
    [DataContract(IsReference = true)]
    public class Note : INotifyPropertyChanged
    {
        [DataMember]
        public int id { get; init; }
        [DataMember]
        private string name;
        [DataMember]
        public string Name
        {
            get { return name; }
            set { if (value == "") { name = "Unnamed Note"; } else { name = value; } OnPropertyChanged(nameof(Name)); }
        }

        [DataMember]
        private string text;
        public string Text { 
            get => text;
            set {
                text = value;
                OnPropertyChanged(nameof(Text));
            } }

        [DataMember]
        private string logoPath;
        [DataMember]
        public string LogoPath
        {
            get { return logoPath; }
            private set { if (value == "") { logoPath = "PATH TO DEFAULT LOGO"; } else { logoPath = value; } OnPropertyChanged(nameof(LogoPath)); }
        }

        [DataMember]
        public DateOnly CreationDate { get; init; }
        [DataMember]
        public DateOnly ModificationDate { get; private set; }
        [DataMember]
        public List<NoteImage> ImageList { get; private set; }
        [DataMember]
        public List<User> Collaborators { get; private set; }
        [DataMember]
        public List<User> Editors { get; private set; }
        [DataMember]
        public User Owner { get; private set; }
        public bool isfavorite;
        public bool IsFavorite 
        { 
            get => isfavorite;
            set
            {
                isfavorite = value;
                OnPropertyChanged(nameof(IsFavorite));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public Note(int initId,string name, string logoPath, User owner)
        {
            id = initId;
            Name = name;
            LogoPath = logoPath;
            CreationDate = DateOnly.FromDateTime(DateTime.Now);
            ModificationDate = DateOnly.FromDateTime(DateTime.Now);
            ImageList = new List<NoteImage>();
            Collaborators = new List<User>();
            Editors = new List<User>() { owner };
            Owner = owner;
        }

        public override string ToString() => $"Note -> Name: {Name}\nLogoPath: {LogoPath}";

        /// <summary>
        /// vérifier si l'utilisateur est le propriétaire de la note
        /// </summary>
        public bool VerifyOwner(User user) 
        {
            if (user == Owner) { return true; }
            else { throw new NotAllowedException("User is not the owner"); }
        }

        /// <summary>
        /// ajouter une image à la note
        /// </summary>
        /// <param name="imageLink"></param>
        /// <param name="position"></param>
        public void AddImage(string imageLink, List<int> position)
        {
            string newname = (ImageList.Count + 1).ToString();
            ImageList.Add(new NoteImage(newname, imageLink, position));
        }

        /// <summary>
        /// supprimer une image de la note
        /// </summary>
        /// <param name="name"></param>
        /// <exception cref="NotFoundException"></exception>
        public void RemoveImage(string name)
        {
            foreach (NoteImage image in ImageList)
            {
                if (image.Name == name)
                {
                    ImageList.Remove(image);
                    return;
                }
            }
            throw new NotFoundException("Image not found");
        }

        /// <summary>
        /// ajouter du texte à la note
        /// </summary>
        /// <param name="text"></param>
        public void AddText( User user, string text)
        {
            if (user == Owner || Editors.Contains(user)) { ModificationDate = DateOnly.FromDateTime(DateTime.Now); }
            else { throw new NotAllowedException("User is not the owner or an editor"); }
            Text = Text + "\n" + text;
        }

        /// <summary>
        /// vérifier si l'utilisateur est un éditeur de la note
        /// </summary>
        public bool VerifyPrivilege(User user)
        {
            return Editors.Contains(user);
        }

        /// <summary>
        /// ajouter un utilisateur en tant que coopérateur
        /// </summary>
        public void AddCollaborator(User owner, User user)
        {
            if (VerifyOwner(owner)) { Collaborators.Add(user); }
            user.NoteList.Add(this);
        }

        /// <summary>
        /// supprimer un utilisateur en tant que coopérateur
        /// </summary>
        public void RemoveCollaborator(User owner, User user)
        {
            if (VerifyOwner(owner)) { Collaborators.Remove(user); }
            user.NoteList.Remove(this);
        }

        /// <summary>
        /// passer un coopérateur en tant qu'éditeur
        /// </summary>
        public void AddEditor(User owner, User user)
        {
            if (Editors.Contains(user)) { throw new AlreadyExistException("user already in editors."); }
            if (VerifyOwner(owner)) { Editors.Add(user); }
        }

        /// <summary>
        /// supprimer un coopérateur de la liste des éditeurs
        /// </summary>
        public void RemoveEditor(User owner, User user)
        {
            if (!Editors.Contains(user)) { throw new NotFoundException("user not found in editors."); }
            if (VerifyOwner(owner)) { Editors.Remove(user); }
        }

        /// <summary>
        /// changer le nom de la note
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newName"></param>
        public void ChangeName(User user, string newName)
        {
            if (VerifyOwner(user)) { Name = newName; }
        }

        /// <summary>
        /// changer le logo de la note
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newLogoPath"></param>
        public void ChangeLogo(User user, string newLogoPath)
        {
            if (VerifyOwner(user)) { LogoPath = newLogoPath; }
        }
    }
}
