﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [DataContract]
    public class Theme
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<string> ColorList { get; private set; }

        public Theme(string name, List<string> colorList)
        {
            Name = name;
            ColorList = colorList;
        }

        public override string ToString() => $"name: {Name}\ncolor 1: {ColorList[0]}\ncolor 2: {ColorList[1]}\ncolor 3: {ColorList[2]}\n";

        /// <summary>
        /// Change a specific color of the theme.
        /// </summary>
        public void ChangeColor(string color, string newColor)
        {
            for (int i = 0; i < ColorList.Count; i++)
            {
                if (ColorList[i] == color)
                {
                    ColorList[i] = newColor;
                }
            }
        }
    }
}
