﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [DataContract]
    public class NoteImage
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageLink { get; set; }
        [DataMember]
        public List<int> Position { get; set; }

        public NoteImage(string name, string imageLink, List<int> position)
        {
            Name = name;
            ImageLink = imageLink;
            Position = position;
        }

        public override string ToString() => $"image -> name: {Name}\nlink: {ImageLink}";
    }
}
