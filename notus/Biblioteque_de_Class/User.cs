﻿using Microsoft.VisualBasic.FileIO;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Biblioteque_de_Class
{
    [DataContract]
    public class User : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [DataMember]
        private string username;
        public string Username
        {
            get { return username; }
            set 
            {
                if (username == value) 
                    return; 
                username = value;
                OnPropertyChanged(nameof(Username));
            }
        }
        [DataMember]
        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                if (email == value)
                    return;
                email = value;
                OnPropertyChanged(nameof(Email));
            }
        }
        [DataMember]
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                if (password == value)
                    return;
                password = value;
                OnPropertyChanged(nameof(Password));
            }
        }
        [DataMember]
        private string picture;
        public string Picture
        {
            get { return picture; }
            set
            {
                if (picture == value)
                    return;
                picture = value;
                OnPropertyChanged(nameof(Picture));
            }
        }
        [DataMember]
        private Theme useTheme;
        public Theme UseTheme
        {
            get { return useTheme; }
            set
            {
                if (useTheme == value)
                    return;
                useTheme = value;
                OnPropertyChanged(nameof(UseTheme));
            }
        }
        [DataMember]
        private List<Note> noteList;
        public List<Note> NoteList
        {
            get { return noteList; }
            set
            {
                if (noteList == value)
                    return;
                noteList = value;
                OnPropertyChanged(nameof(NoteList));
            }
        }
        [DataMember]
        private List<Tags> tagList;
        public List<Tags> TagList
        {
            get { return tagList; }
            set
            {
                if (tagList == value)
                    return;
                tagList = value;
                OnPropertyChanged(nameof(TagList));
            }
        }
        [DataMember]
        private List<Note> favList;
        public List<Note> FavList
        {
            get { return favList; }
            set
            {
                if (favList == value)
                    return;
                favList = value;
                OnPropertyChanged(nameof(FavList));
            }
        }
        [DataMember(EmitDefaultValue = false)]
        private bool isConnected;
        public bool IsConnected
        {
            get { return isConnected; }
            set
            {
                if (isConnected == value)
                    return;
                isConnected = value;
                OnPropertyChanged(nameof(IsConnected));
            }
        }
        [DataMember]
        private Dictionary<Note, List<Tags>> noteTagged;
        public Dictionary<Note, List<Tags>> NoteTagged
        {
            get { return noteTagged; }
            set
            {
                if (noteTagged == value)
                    return;
                noteTagged = value;
                OnPropertyChanged(nameof(NoteTagged));
            }
        }
        [DataMember]
        private List<Theme> addedTheme;
        public List<Theme> AddedTheme
        {
            get { return addedTheme; }
            set
            {
                if (addedTheme == value)
                    return;
                addedTheme = value;
                OnPropertyChanged(nameof(AddedTheme));
            }
        }

        public User(string username, string email, string password)
        {
            Username = username;
            Email = email;
            Password = password;
            Picture = "defaultpicture.png";
            UseTheme = new("", ",,".Split(',').ToList());
            NoteList = new List<Note>();
            TagList = new List<Tags>();
            FavList = new List<Note>();
            NoteTagged = new Dictionary<Note, List<Tags>>();
            AddedTheme = new List<Theme>();
        }
        public override string ToString() => $"username: {Username}\nemail: {Email}\npassword: {Password}\nOwned notes: {NoteList.Count}";

        /// <summary>
        /// rechercher une note dans la liste de note de l'utilisateur et la liste de note favoris de l'utilisateur
        /// </summary>
        public List<Note> SearchNoteByName(List<Note> toResearchIntoList, string name)
        {
            List<Note> searchedNotes = new List<Note>();
            string search = name.ToLower();
            foreach (Note note in toResearchIntoList)
            {
                if (note.Name.ToLower().Contains(search))
                {
                    searchedNotes.Add(note);
                }
            }
            return searchedNotes;
        }

        public List<Note> SearchNoteByTag(List<Note> toResearchIntoList, string name)
        {
            List<Note> searchedNotes = new();
            Tags tagtoresearchby = GetTagByName(name);
            foreach(Note note in toResearchIntoList)
            {
                if (NoteTagged[note].Contains(tagtoresearchby))
                {
                    searchedNotes.Add(note);
                }
            }
            return searchedNotes;
        }

        /// <summary>
        /// rechercher un tag dans la liste de tag de l'utilisateur
        /// </summary>
        public List<Tags> SearchTagByName(List<Tags> ToResearchIntoList, string name)
        {
            List<Tags> searchedTags = new List<Tags>();
            string search = name.ToLower();
            foreach (Tags tag in ToResearchIntoList)
            {
                if (tag.Name.ToLower().Contains(search))
                {
                    searchedTags.Add(tag);
                }
            }
            return searchedTags;
        }

        /// <summary>
        /// rechercher par date de création ou de modification
        /// </summary>
        /// <param name="ToResearchIntoList"></param>
        /// <param name="name"></param>
        /// <param name="FirstDate"></param>
        /// <param name="SecondeDate"></param>
        /// <returns></returns>
        public List<Note> SearchNoteByDate(List<Note> ToResearchIntoList, string name, DateOnly FirstDate, DateOnly SecondeDate)
        {
            List<Note> searchedNotes = new List<Note>();
            string search = name.ToLower();
            foreach (Note note in ToResearchIntoList)
            {
                if (note.Name.ToLower().Contains(search) && note.CreationDate >= FirstDate && note.CreationDate <= SecondeDate || note.Name.ToLower().Contains(search) && note.ModificationDate >= FirstDate && note.ModificationDate <= SecondeDate)
                {
                    searchedNotes.Add(note);
                }
            }
            return searchedNotes;
        }

        /// <summary>
        /// rechercher par nom de la note
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public Note GetNoteByName(string name)
        {
            foreach (Note note in NoteList)
            {
                if (note.Name == name)
                {
                    return note;
                }
            }
            throw new NotFoundException("Note not found");
        }

        /// <summary>
        /// rechercher par nom du tag
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public Tags GetTagByName(string name)
        {
            foreach (Tags tag in TagList)
            {
                if (tag.Name == name)
                {
                    return tag;
                }
            }
            throw new NotFoundException("Tag not found");
        }

        /// <summary>
        /// ajouter une note dans la liste de note favorite de l'utilisateur
        /// </summary>
        public void AddFavorite(Note note)
        {
            if (FavList.Contains(note))
            {
                throw new AlreadyExistException("Note already in favorites");
            }
            FavList.Add(note);
            note.IsFavorite = true;
        }

        /// <summary>
        /// supprimer une note dans la liste de note favorite de l'utilisateur
        /// </summary>
        public void RemoveFavorite(Note note)
        {
            if (FavList.Contains(note))
            {
                FavList.Remove(note);
                note.IsFavorite = false;
            }
            else
            {
                throw new NotFoundException("Note not found");
            }
        }

        /// <summary>
        ///creer une note
        /// </summary>
        public Note CreateNote(string name, string imagePath)
        {
            if (name != "")
            {
                foreach (Note existingNote in NoteList)
                {
                    if (existingNote.Name == name)
                    {
                        throw new AlreadyExistException("Note already exists");
                    }
                }
            }
            Note note;
            if (NoteList.Count == 0) { note = new Note(0, name, imagePath, this); }
            else { note = new Note(NoteList[NoteList.Count - 1].id + 1, name, imagePath, this); }
            NoteList.Add(note);
            NoteTagged.Add(note, new List<Tags>());
            return note;
        }

        /// <summary>
        /// supprimer une note
        /// </summary>
        public void DeleteNote(Note note)
        {
            note.VerifyOwner(this);
            foreach (Note existingNote in NoteList)
            {
                if (existingNote == note)
                {
                    NoteList.Remove(existingNote);
                    NoteTagged.Remove(existingNote);
                    return;
                }
            }
            throw new NotFoundException("Note not found");
        }

        /// <summary>
        /// creer un tag
        /// </summary>
        public void CreateTag(string name, string color)
        {
            foreach (Tags tag in TagList)
            {
                if (tag.Name == name)
                {
                    throw new AlreadyExistException("Tag already exists");
                }
            }
            TagList.Add(new Tags(name, color));
        }

        /// <summary>
        /// supprimer un tag
        /// </summary>
        public void DeleteTag(Tags tagtodelete)
        {
            foreach (Tags tag in TagList)
            {
                if (tag == tagtodelete)
                {
                    TagList.Remove(tag);
                    return;
                }
            }
            throw new NotFoundException("Tag not found");
        }

        public void EditTagName(Tags tag, string newName)
        {
            if (!TagList.Contains(tag))
            {
                throw new NotFoundException("Tag not found");
            }
            else
            {
                foreach (Tags existingTag in TagList)
                {
                    if (existingTag.Name == newName)
                    {
                        throw new AlreadyExistException("Tag already exists");
                    }
                }
            }
            tag.Name = newName;
        }

        public void EditTagColor(Tags tag, string newColor)
        {
            if (!TagList.Contains(tag))
            {
                throw new NotFoundException("Tag not found");
            }
            tag.Color = newColor;
        }

        /// <summary>
        /// ajouter un tag a une note
        /// </summary>
        public void AddTagFromNoteList(Note note, Tags tagToAdd)
        {
            if (!TagList.Contains(tagToAdd))
            {
                throw new NotFoundException("Tag not found");
            }
            if (!NoteList.Contains(note))
            {
                throw new NotFoundException("Note not found");
            }
            NoteTagged[note].Add(tagToAdd);
        }

        /// <summary>
        /// supprimer un tag a une note
        /// </summary>
        public void RemoveTagFromNoteList(Note note, Tags tagToRemove)
        {
            if (!TagList.Contains(tagToRemove))
            {
                throw new NotFoundException("Tag not found");
            }
            if (!NoteList.Contains(note))
            {
                throw new NotFoundException("Note not found");
            }
            NoteTagged[note].Remove(tagToRemove);
        }

        /// <summary>
        /// modifier le nom d'une note
        /// </summary>
        /// <param name="note"></param>
        /// <param name="newname"></param>
        /// <exception cref="AlreadyUsedException"></exception>
        public void SetNoteName(Note note, string newname)
        {
            if (!NoteList.Contains(note))
            {
                throw new NotFoundException("Note not found");
            }
            foreach(Note n in NoteList)
            {
                if(n.Name == newname)
                {
                    throw new AlreadyUsedException("This name is already used");
                }
            }
            note.Name = newname;
        }

        /// <summary>
        /// modifier le mot de passe de l'utilisateur
        /// </summary>
        /// <param name="newPassword"></param>
        /// <exception cref="AlreadyExistException"></exception>
        /// <exception cref="NotAllowedException"></exception>
        public void ChangePassword(string newPassword)
        {
            if (newPassword.Length < 8) { throw new NotAllowedException("this password is too short."); }
            if (Password == HashCodeModel.GetSHA256Hash(newPassword).ToString())
            {
                throw new AlreadyUsedException("this password is already used.");
            }
            else
            {
                Password = HashCodeModel.GetSHA256Hash(newPassword).ToString();
            }
        }

        /// <summary>
        /// modifier le theme de l'utilisateur
        /// </summary>
        /// <param name="theme"></param>
        /// <exception cref="AlreadyExistException"></exception>
        public void ChangeTheme(Theme theme)
        {
            if (UseTheme.Name == theme.Name)
            {
                throw new AlreadyExistException("this theme is already selected.");
            }
            UseTheme = theme;
        }

        public void ChangeThemeName(Theme theme, string newName)
        {
            foreach (Theme existingTheme in AddedTheme)
            {
                if (existingTheme.Name == newName)
                {
                    throw new AlreadyExistException("this theme is already existing.");
                }
            }
            if (theme.Name == newName)
            {
                throw new AlreadyExistException("this theme is already selected.");
            }
            theme.Name = newName;
        }

        public void ChangeThemeColors(Theme theme, List<string> newColor)
        {
            int compteurSameColor = 0;
            for(int i = 0; i < theme.ColorList.Count; i++)
            {
                if (theme.ColorList[i] == newColor[i])
                {
                    compteurSameColor++;
                }
            }
            if (compteurSameColor == theme.ColorList.Count)
            {
                throw new AlreadyExistException("this theme those colors");
            }
            if (theme.ColorList.Count != newColor.Count)
            {
                throw new NotFoundException("this theme doesn't have the same number of colors");
            }
            for (int i = 0; i < theme.ColorList.Count; i++)
            {
                theme.ChangeColor(theme.ColorList[i], newColor[i]);
            }
        }

        /// <summary>
        /// ajouter un theme dans la liste de theme
        /// </summary>
        public void AddTheme(Theme theme)
        {
            foreach (Theme existingTheme in AddedTheme)
            {
                if (existingTheme.Name == theme.Name)
                {
                    throw new AlreadyExistException("Theme already used.");
                }
            }
            AddedTheme.Add(theme);
        }

        /// <summary>
        /// supprimer un theme dans la liste de theme
        /// </summary>
        public void RemoveTheme(Theme theme)
        {
            if (AddedTheme.Contains(theme))
            {
                AddedTheme.Remove(theme);
            }
            else
            {
                throw new NotFoundException("Theme not found.");
            }
        }

        /// <summary>
        /// recuperer un theme dans la liste de theme ajouté par l'utilisateur
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public Theme GetTheme(string name)
        {
            foreach (Theme theme in AddedTheme)
            {
                if (theme.Name == name)
                {
                    return theme;
                }
            }
            throw new NotFoundException("Theme not found.");
        }

        /// <summary>
        /// modifier l'email de l'utilisateur
        /// </summary>
        /// <param name="newEmail"></param>
        /// <exception cref="AlreadyExistException"></exception>
        public void ChangeEmail(string newEmail)
        {
            if (newEmail.Length < 3 ) { throw new NotAllowedException("this is not a mail address."); }
            if (Email == newEmail) { throw new AlreadyUsedException("this email is the same."); }
            Email = newEmail;
        }

        /// <summary>
        /// changer la photo de profil de l'utilisateur
        /// </summary>
        /// <param name="path"></param>
        public void ChangeProfilePicture(string path)
        {
            if (path.Length < 3)
            {
                Picture = "default.png";
            }
            else
            {
                List<string> picture = new();
                picture = path.Split('.').ToList();
                string extension = picture.Last();
                if (extension != "png" && extension != "jpg" && extension != "jpeg")
                {
                    throw new NotFoundException("this extension is not allowed.");
                }
                Picture = path;
            }
        }

        public void SetDefaultTheme(Theme theme)
        {
            UseTheme = theme;
        }
    }
}