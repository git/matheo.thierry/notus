﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Biblioteque_de_Class
{
    [DataContract]
    public class Database
    {
        [DataMember]
        public List<Logo> DefaultLogoList { get; private set; }
        [DataMember]
        public List<Theme> ThemeList { get; private set; }
        [DataMember]
        public List<User> Users;
        public List<User> UserList
        {
            get => Users; private set
            {
                Users = value;
            }
        }

        public Database()
        {
            DefaultLogoList = new List<Logo>();
            ThemeList = new List<Theme>();
            UserList = new List<User>();
        }

        public void SetDefaultLogoList(List<Logo> defaultLogoList) {  DefaultLogoList = defaultLogoList; }
        public void SetDefaultThemeList(List<Theme> defaultThemeList) { ThemeList = defaultThemeList; }
        public void SetUserList(List<User> userList) { UserList = userList; }

        /// <summary>
        /// recherche un utilisateur dans la liste d'utilisateur
        /// </summary>
        public User SearchUser(string name)
        {
            foreach(User user in UserList)
            {
                if (user.Username == name)
                {
                    return user;
                }
            }
            throw new NotFoundException("No user found with this username.");
        }

        /// <summary>
        /// récupérer le lien d'un logo
        /// </summary>
        public string? GetLogoLink(string name)
        {
            foreach (Logo logo in DefaultLogoList)
            {
                if (logo.Name == name) { return logo.LogoLink; }
            }
            throw new NotFoundException("No logo link found.");
        }

        /// <summary>
        /// récupérer un utilisateur
        /// </summary>
        public User GetUser(string name)
        {
            foreach (User user in UserList)
            {
                if (user.Username == name)
                {
                    return user;
                }
            }
            throw new NotFoundException("No user found with this username.");
        }

        /// <summary>
        /// comparer le mot de passe entré avec celui de l'utilisateur
        /// </summary>
        public static bool ComparePassword(User user, string? password) => user.Password == password;

        /// <summary>
        /// rechercher un mail dans la liste d'utilisateur
        /// </summary>
        public bool FindEmail(string email)
        {
            foreach (User user in UserList)
            {
                if (string.Equals(user.Email, email))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// ajouter un utilisateur dans la liste d'utilisateur
        /// </summary>
        public void AddUser(User user)
        {
            foreach (User existingUser in UserList)
            {
                if (existingUser.Username == user.Username)
                {
                    throw new AlreadyUsedException("Username already used.");
                }
                else if (user.Email != "")
                {
                    if (existingUser.Email == user.Email)
                    {
                        throw new AlreadyUsedException("Email already used.");
                    }
                }
            }
            UserList.Add(user);
            user.CreateNote("", ""); // création d'une note vide pour l'utilisateur
        }

        /// <summary>
        /// supprimer un utilisateur dans la liste d'utilisateur
        /// </summary>
        public void RemoveUser(User user)
        {
            if (UserList.Contains(user))
            {
                UserList.Remove(user);
            }
            else
            {
                throw new NotFoundException("User not found.");
            }
        }

        /// <summary>
        /// ajouter un theme dans la liste de theme
        /// </summary>
        public void AddTheme(Theme theme)
        {
            foreach (Theme existingTheme in ThemeList)
            {
                if (existingTheme.Name == theme.Name)
                {
                    throw new AlreadyExistException("Theme already Existing.");
                }
            }
            ThemeList.Add(theme);
        }

        /// <summary>
        /// récupérer un theme
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public Theme GetTheme(string name)
        {
            foreach (Theme theme in ThemeList)
            {
                if (theme.Name == name)
                {
                    return theme;
                }
            }
            throw new NotFoundException("Theme not found.");
        }

        /// <summary>
        /// changer le pseudo d'un utilisateur
        /// </summary>
        /// <param name="user"></param>
        /// <param name="newUsername"></param>
        /// <exception cref="AlreadyUsedException"></exception>
        public void ChangeUsername(User user, string newUsername) 
        {
            foreach(User existingUser in UserList)
            {
                if(existingUser.Username == newUsername)
                {
                    throw new AlreadyUsedException("this username is already used.");
                }
            }
            user.Username = newUsername;
        }

        /// <summary>
        /// vérifier si le nom du theme n'est pas déjà pris
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool VerifThemeNameNotTaken(string name)
        {
            foreach (Theme theme in ThemeList)
            {
                if (theme.Name == name)
                {
                    return false;
                }
            }
            return true;
        }
    }
}