﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    public interface IManager
    {
        public void SaveDatabaseData(List<User> UserList);
        public void SaveDefaultData(List<Theme> DefaultThemeList, List<Logo> DefaultLogoList);
        public List<User> LoadDatabaseData();
        public List<Theme> LoadDefaultTheme();
        public List<Logo> LoadDefaultLogo();
    }
}
