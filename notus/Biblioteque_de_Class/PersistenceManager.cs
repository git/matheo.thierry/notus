﻿using Microsoft.VisualBasic;

namespace Biblioteque_de_Class
{
    public class PersistenceManager
    {
        private Database db = new Database();

        private readonly IManager persistence;

        public PersistenceManager(IManager pers)
        {
            persistence = pers;
        }
        
        public void SaveDatabaseData(Database database)
        {
            persistence.SaveDatabaseData(database.UserList);
        }

        public void SaveDefaultData(Database database)
        {
            persistence.SaveDefaultData(database.ThemeList, database.DefaultLogoList); 
        }

        public  Database LoadDatabaseData()
        {
            db.SetUserList(persistence.LoadDatabaseData());
            db.SetDefaultThemeList(persistence.LoadDefaultTheme());
            db.SetDefaultLogoList(persistence.LoadDefaultLogo());
            return db;
        }

        public Database GetOnlyDatabaseUser()
        {
            db.SetUserList(persistence.LoadDatabaseData());
            return db;
        }

        public Database GetOnlyDatabaseDefaultTheme()
        {
            db.SetDefaultThemeList(persistence.LoadDefaultTheme());
            return db;
        }

        public Database GetOnlyDatabaseDefaultLogo()
        {
            db.SetDefaultLogoList(persistence.LoadDefaultLogo());
            return db;
        }
    }
}
