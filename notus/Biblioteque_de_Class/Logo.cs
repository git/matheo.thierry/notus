﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [DataContract]
    public class Logo
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string LogoLink { get; set; }

        public Logo(string name, string logoLink)
        {
            Name = name;
            LogoLink = logoLink;
        }

        public override string ToString() => $"Logo -> Name: {Name}\nLink: {LogoLink}";
    }
}
