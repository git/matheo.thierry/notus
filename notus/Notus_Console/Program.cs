﻿using Biblioteque_de_Class;
using Notus_Persistance;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

// load database
PersistenceManager manager = new PersistenceManager(new Stub());
Database db = manager.LoadDatabaseData();

//save database
PersistenceManager managerSave = new(new ToXML());

// initialization zone==============================================================================

bool arreter = true;
bool continuerboucle = false;
bool menu = true, connection = false, inscription = false;
bool note=false, tags=false, para=false, paraCompte=false, theme=false;

// déclaration d'un user qui sera utiliser pour servir de personne connecté dans l'app
User u = new("", "", "");
User uvide = new("", "", "") { IsConnected = false };

// déclaration d'une note qui sera utiliser pour servir de note selectionnée
Note n = new(uvide.NoteList.Count,"","",uvide);

List<Note> researchlist = new();

/*
 *  starting menu:
 *      connection
 *      inscription
 *  
 *  menu principal:
 *      note (créer, modifier, supprimer, partager, rechercher)
 *      tags (créer, modifier, supprimer)
 *      parametre (compte (modifier, supprimer), theme ( créer, modifier, supprimer))
 *      se déconnecter
 */


// factorisation zone ================================================================================

// pour faire un choix par defaut non
bool Choix_DefaultNon()
{
    continuerboucle = false;
    while (!continuerboucle)
    {
        Console.WriteLine("\nContinuer ? (o/N)");
        switch (Console.ReadLine())
        {
            case "O":
                return true;
            case null:
                return false;
            case "o":
                return true;
            case "n":
                return false;
            default:
                Console.WriteLine("\nEntrez un choix valide.\n");
                continuerboucle = true;
                break;
        }
    }
    return false;
}

// pour selectionner une note dans le menu note
bool Choix_Note()
{
    foreach (Note notetodisplay in u.NoteList)
    {
        Console.WriteLine("- " + notetodisplay.Name);
    }
    continuerboucle = true;
    while (continuerboucle)
    {
        Console.WriteLine("Quel note voulez vous selectionner ? (entrez le nom de la note complet)");
        string? nom = Console.ReadLine();
        if (nom == null) { continue; }
        foreach (Note note in u.NoteList)
        {
            if (note.Name == nom)
            {
                n = note;
                return true;
            }
        }
        Console.WriteLine("\nEntrez un choix valide.\n");
        Console.WriteLine("Voulez vous reessayer ? (o/N)");
        continuerboucle = Choix_DefaultNon();
    }
    return false;
}

bool CouleurValide(string couleur)
{
    int correct = 0;
    if (couleur[0] == '#' && couleur.Length == 7)
    {
        for (int i = 1; i < 7-1; i++)
        {
            if (couleur[i] >= '0' && couleur[i] <= '9' || couleur[i] >= 'A' && couleur[i] <= 'F' || couleur[i] >= 'a' && couleur[i] <= 'f')
            {
                correct++;
            }
        }
        if (correct == 6)
        {
            return true;
        }
    }
    return false;
}

bool IsValidEmail(string email)
{
    return new EmailAddressAttribute().IsValid(email);
}

List<string> Choix_CouleursToTheme()
{
    Console.WriteLine("Choisez les couleurs respetivement fond, text, boutton en hexa (ex: #FFFFFF) séparé par des epaces");
    List<string> listcouleurs = new();
    string? couleur = Console.ReadLine();
    if (couleur == null) { return new List<string>(); }
    listcouleurs = couleur.Split(' ').ToList();
    foreach (string coul in listcouleurs)
    {
        if (!CouleurValide(coul))
        {
            Console.WriteLine("\nEntrez un choix valide. pour "+ coul +"\n");
            return new List<string>();
        }
    }
    return listcouleurs;
}

// program zone =====================================================================================
while (arreter)
{
    //starting menu
    while (menu)
    {
        Console.WriteLine("\n|--------------------------------------|");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|          starting menu               |");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|--------------------------------------|--------|");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|  1 / - connection -                           |");
        Console.WriteLine("|  2 / - inscription -                          |");
        Console.WriteLine("|  3 / - arreter le program -                   |");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|-----------------------------------------------|\n");
        Console.WriteLine("rentrez votre choix.");
        switch (Console.ReadLine())
        {
            case "1": ///Connexion
                connection = true; break;

            case "2":///Creer un compte
                inscription = true; break;
            case "3":///Arreter le program
                arreter = false;
                menu = false;
                break;
            default:
                Console.WriteLine("\nEntrez un choix valide.\n");
                break;
        }

        //connection
        while (connection)
        {
            connection = false;
            Console.WriteLine("\nEntrez un nom : ");
            string? nom = Console.ReadLine();
            if (nom == null) { continue; }
            Console.WriteLine("\nEntrez un password :");
            string? password = Console.ReadLine();
            if (password == null) { continue; }

            //test si l'utilisateur existe et si le password est bon
            try
            {
                u = db.GetUser(nom);
            }
            catch (AlreadyUsedException ex)
            {
                Console.WriteLine(ex.Message);
                connection = true;
            }
            if (!connection)
            {

                if (Database.ComparePassword(u, HashCodeModel.GetSHA256Hash(password).ToString()))
                {
                    u.IsConnected = true;
                    Console.WriteLine("\nConnection réussie !\n");
                    menu = false;
                    break;
                }
                else
                {
                    Console.WriteLine("\nWrong PassWord !\n");
                    connection = true;
                    continuerboucle = true;
                    u = uvide;
                }
            }

            // demander si il veut continuer de ce connecter
            connection = Choix_DefaultNon();
        }



        //inscription
        while (inscription)
        {
            Console.WriteLine("\nEntrez un nom :");
            string? nom = Console.ReadLine();
            if (nom == null) { continue; }
            Console.WriteLine("\nEntrez un password :");
            string? password = Console.ReadLine();

            //test si l'utilisateur n'est pas déjà dans la base de donnée et ajout dans la base de donnée
            if (password == null) { continue; }
            try
            {
                u = db.GetUser(nom);
            }
            catch (AlreadyUsedException)
            {
                u = new User(nom, "", HashCodeModel.GetSHA256Hash(password).ToString());
                db.AddUser(u);
                db.GetUser(nom).IsConnected = true;
                Console.WriteLine("\nConnection réussie !\n");
                menu = false;
                break;
            }
            Console.WriteLine("\nNom d'utilisateur déjà utilisé. \n");

            // demander si il veut continuer de ce connecter
            inscription = Choix_DefaultNon();

        }
    }

    //menu principal
    while (u.IsConnected)
    {
        Console.WriteLine("\n|--------------------------------------|");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|                menu                  |");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|--------------------------------------|--------|");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|  1/ - note -                                  |");
        Console.WriteLine("|  2/ - tags -                                  |");
        Console.WriteLine("|  3/ - paramêtres -                            |");
        Console.WriteLine("|  4/ - se déconnecter -                        |");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|-----------------------------------------------|\n");
        Console.WriteLine("rentrez votre choix.");
        switch (Console.ReadLine())
        {
            case "1":
                note = true;
                foreach (Note notetodisplay in u.NoteList)
                {
                    Console.WriteLine("- " + notetodisplay.Name);
                }
                break;
            case "2":
                tags = true;
                break;
            case "3":
                para = true;
                break;
            case "4":
                menu = true;
                Console.WriteLine("\ndéconnecté! \n");
                u.IsConnected = false;
                u = uvide;
                break;
            default:
                Console.WriteLine("\nEntrez un choix valide.\n");
                break;
        }

        while (note)
        {
            Console.WriteLine("\n|--------------------------------------|");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|           menu - note                |");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|--------------------------------------|--------|");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|  1/ - créer une note -                        |");
            Console.WriteLine("|  2/ - modifier une note -                     |");
            Console.WriteLine("|  3/ - supprimer une note -                    |");
            Console.WriteLine("|  4/ - écrire dans une note -                  |");
            Console.WriteLine("|  5/ - ajouter une image -                     |");
            Console.WriteLine("|  6/ - supprimer une image -                   |");
            Console.WriteLine("|  7/ - partager note -                         |");
            Console.WriteLine("|  8/ - supprimer collaborateur -               |");
            Console.WriteLine("|  9/ - ajouter un éditeur -                    |");
            Console.WriteLine("|  10/ - supprimer un éditeur -                 |");
            Console.WriteLine("|  11/ - ajouter tag -                          |");
            Console.WriteLine("|  12/ - enelver tag -                          |");
            Console.WriteLine("|  13/ - rechercher une note -                  |");
            Console.WriteLine("|  14/ - retour -                               |");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|-----------------------------------------------|\n");
            Console.WriteLine("note actuelle : " + n.Name);
            Console.WriteLine("rentrez votre choix.");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("\nEntrez le nom de la note :");
                    string? wantedNameNote = Console.ReadLine();
                    if (wantedNameNote == null) { continue; }
                    try
                    {
                        n = u.CreateNote(wantedNameNote, "");
                    }catch(AlreadyUsedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    Console.WriteLine("\nNote créée !\n");
                    break;
                case "2":
                    if (!Choix_Note()) { break; }
                    Console.WriteLine("Voulez vous modifier le nom de la note ? (o/N)");
                    if (Choix_DefaultNon())
                    {
                        Console.WriteLine("\nEntrez le nouveau nom de la note :");
                        string? wantedNameNote2 = Console.ReadLine();
                        if (wantedNameNote2 == null) { continue; }
                        try
                        {
                            n.ChangeName(u, wantedNameNote2);
                        }
                        catch (NotAllowedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("\nNom modifié !\n");
                    }
                    break;
                case "3":
                    if (!Choix_Note()) { break; }
                    Console.WriteLine("Voulez vous vraiment supprimer la note ? (o/N)");
                    if (Choix_DefaultNon())
                    {
                        try
                        {
                            u.DeleteNote(n);
                        }
                        catch (NotAllowedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("\nNote supprimée !\n");
                    }
                    break;
                case "4":
                    if (!Choix_Note()) { break; }
                    Console.WriteLine(n.Text); // affiche le texte de la note
                    if (!n.VerifyPrivilege(u)) { break; }
                    Console.WriteLine("\nEntrez le texte à ajouter");
                    string? wantedTextNote = Console.ReadLine();
                    if (wantedTextNote == null) { continue; }
                    try
                    {
                        n.AddText(u, wantedTextNote);
                    }
                    catch(NotAllowedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "5":
                    if (!Choix_Note()) { break; }
                    if (!n.VerifyPrivilege(u)) { break; }
                    Console.WriteLine("Entrer le chemin de l'image :");
                    string? wantedimage = Console.ReadLine();
                    if (wantedimage == null) { continue; }
                    n.AddImage(wantedimage, new() { 0, 0 });
                    break;
                case "6":
                    if (!Choix_Note()) { break; }
                    if(!n.VerifyPrivilege(u)) { break; }
                    foreach (NoteImage image in n.ImageList)
                    {
                        Console.WriteLine(image.Name + " -> " + image.ImageLink + "\n");
                    }
                    Console.WriteLine("entrez le numéro de l'image à supprimer :");
                    string? wantedimagetodelete = Console.ReadLine();
                    if(wantedimagetodelete == null) { continue; }
                    try
                    {
                        n.RemoveImage(wantedimagetodelete);
                    }
                    catch(NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "7":
                    if (!Choix_Note()) { break; }
                    if (!n.VerifyOwner(u)) {  continue; }
                    User wantedUser;
                    Console.WriteLine("\nEntrez le nom de l'utilisateur à qui partager la note :");
                    string? wantedNameUser = Console.ReadLine();
                    if (wantedNameUser == null) { continue; }
                    try
                    {
                        wantedUser = db.SearchUser(wantedNameUser);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        n.AddCollaborator(u, wantedUser);
                    }
                    catch (NotAllowedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "8": //supp collab
                    User wantedUser2;
                    if (!Choix_Note()) { break; }
                    if (!n.VerifyOwner(u)) { continue; }
                    Console.WriteLine("\n Entrez le nom de l'utilisateur qui doit etre supprimer : ");
                    string? wantedNameUser2 = Console.ReadLine();
                    if (wantedNameUser2 == null) {  continue; }
                    try
                    {
                        wantedUser2 = db.SearchUser(wantedNameUser2);
                    }
                    catch(NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        n.RemoveCollaborator(u, wantedUser2);
                    }
                    catch(NotAllowedException ex) 
                    { 
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "9":// add edit
                    if (!Choix_Note()) { break; }
                    if (!n.VerifyOwner(u)) { continue; }
                    foreach (User user in n.Collaborators)
                    {
                        if( n.Editors.Contains(user)) { continue; }
                        Console.WriteLine( " - " + user.Username );
                    }
                    User UserToAddEdit;
                    Console.WriteLine("Entrer le pseudo de l'utilisateur voulu");
                    string? wantedNameUser3 = Console.ReadLine();
                    if (wantedNameUser3 == null) {  continue; }
                    try
                    {
                        UserToAddEdit = db.SearchUser(wantedNameUser3);
                    }catch(NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        n.AddEditor(u, UserToAddEdit);
                    }
                    catch(AlreadyExistException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "10":// supp edit
                    if (!Choix_Note()) { break; }
                    if (!n.VerifyOwner(u)) { continue; }
                    User UserToDeleteEdit;
                    Console.WriteLine("Entrer le pseudo de l'utilisateur voulu");
                    string? wantedNameUser4 = Console.ReadLine();
                    if (wantedNameUser4 == null) { continue; }
                    try
                    {
                        UserToDeleteEdit = db.SearchUser(wantedNameUser4);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        n.RemoveEditor(u, UserToDeleteEdit);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "11":
                    Tags tagtoadd; 
                    if (!Choix_Note()) { break; }
                    Console.WriteLine("\nEntrez le nom du tag à ajouter :");
                    string? wantedNameTag2 = Console.ReadLine();
                    if (wantedNameTag2 == null) { continue; }
                    try
                    {
                        tagtoadd = u.GetTagByName(wantedNameTag2);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        u.AddTagFromNoteList(n, tagtoadd);
                    }
                    catch (NotAllowedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "12":// enelver tag
                    if (!Choix_Note()) { break; }
                    Console.WriteLine("\n Entrez le nom du tag à supprimer");
                    string? wantedNameTag3 = Console.ReadLine();
                    if (wantedNameTag3 == null) { continue; }
                    try
                    {
                        tagtoadd = u.GetTagByName(wantedNameTag3);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        u.RemoveTagFromNoteList(n, tagtoadd);
                    }
                    catch (NotAllowedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "13":
                    List<Note> wantedNotes = new List<Note>();

                    Console.WriteLine("\nEntrez le nom de la note à rechercher :");
                    string? wantedNameNote3 = Console.ReadLine();
                    if (wantedNameNote3 == null) { continue; }
                    Console.WriteLine("Voulez vous faire une recherche avancé ? (o/N)");
                    if (Choix_DefaultNon())
                    {
                        Console.WriteLine("Voulez vous chercher dans les favoris ? (o/N)");
                        if (Choix_DefaultNon())
                        {
                            wantedNotes = u.FavList;
                        }
                        else
                        {
                            wantedNotes = u.NoteList;
                        }
                        Console.WriteLine("Voulez vous chercher par date seul '1' ou par fourchette de date '2' ? autre pour continuer");
                        switch (Console.ReadLine())
                        {
                            case "1":
                                Console.WriteLine("Entrez la date de la note à rechercher :");
                                string? line = Console.ReadLine();
                                if (line == null) { continue; }
                                DateOnly date1;
                                while (!DateOnly.TryParseExact(line, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out date1))
                                {
                                    Console.WriteLine("Invalid date, please retry");
                                    line = Console.ReadLine();
                                }
                                wantedNotes = u.SearchNoteByDate(wantedNotes, wantedNameNote3, date1, date1);
                                if (wantedNotes == null) { Console.WriteLine("Aucun resultat !"); break; }
                                foreach (Note notetodisplay in wantedNotes)
                                {
                                    Console.WriteLine(" - " + notetodisplay.Name);
                                }
                                break;

                            case "2":
                                Console.WriteLine("Entrez la date de début de la fourchette de date de la note à rechercher :");
                                string? line2 = Console.ReadLine();
                                if (line2 == null) { continue; }
                                DateOnly date2;
                                while (!DateOnly.TryParseExact(line2, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out date2))
                                {
                                    Console.WriteLine("Invalid date, please retry");
                                    line2 = Console.ReadLine();
                                }
                                Console.WriteLine("Entrez la date de début de la fourchette de date de la note à rechercher :");
                                string? line3 = Console.ReadLine();
                                if (line3 == null) { continue; }
                                DateOnly date3;
                                while (!DateOnly.TryParseExact(line3, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out date3))
                                {
                                    Console.WriteLine("Invalid date, please retry");
                                    line3 = Console.ReadLine();
                                }
                                wantedNotes = u.SearchNoteByDate(wantedNotes, wantedNameNote3, date2, date3);
                                if (wantedNotes == null) { Console.WriteLine("Aucun resultat !"); break; }
                                foreach (Note notetodisplay in wantedNotes)
                                {
                                    Console.WriteLine(" - " + notetodisplay.Name);
                                }
                                break;
                            default:
                                break;
                        }
                        Console.WriteLine("\n Voulez vous rechercher par tag (o/N) ? autre pour continuer");
                        if (Choix_DefaultNon())
                        {
                            Console.WriteLine("Plusieurs tags ? (o/N)");
                            if (Choix_DefaultNon())
                            {
                                Console.WriteLine("choisissez les tags à rechercher séparé par un espace :");
                                string? tagstoresearch = Console.ReadLine();
                                if (tagstoresearch == null) { continue; }
                                List<string> listoftagtoresearch = tagstoresearch.Split(' ').ToList();
                            }
                            else
                            {
                                Console.WriteLine("choisissez le tags à rechercher");
                                string? tagstoresearch = Console.ReadLine();
                                if (tagstoresearch == null) { continue; }
                                if (wantedNotes == null) { Console.WriteLine("Aucun resultat !"); break; }
                                try
                                {
                                    wantedNotes = u.SearchNoteByTag(wantedNotes, tagstoresearch);
                                }
                                catch (Exception ex) { Console.WriteLine(ex.Message); break; }
                            }
                            if (wantedNotes == null) { Console.WriteLine("Aucun resultat !"); break; }
                            foreach (Note notetodisplay in wantedNotes)
                            {
                                Console.WriteLine(" - " + notetodisplay.Name);
                            }
                        }
                    }
                    else
                    {
                        wantedNotes = u.SearchNoteByName(wantedNotes, wantedNameNote3);
                        if (wantedNotes == null) { Console.WriteLine("Aucun resultat !"); break; }
                        foreach (Note notetodisplay in wantedNotes)
                        {
                            Console.WriteLine(" - " + notetodisplay.Name);
                        }
                    }
                    break;
                case "14":
                    note = false;
                    break;
                default:
                    Console.WriteLine("\nEntrez un choix valide.\n");
                    break;
            }
        }

        while (tags)
        {
            Console.WriteLine("\n|--------------------------------------|");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|           menu - tags                |");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|--------------------------------------|--------|");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|  1/ - créer tag -                             |");
            Console.WriteLine("|  2/ - modifier un tag -                       |");
            Console.WriteLine("|  3/ - supprimer tag -                         |");
            Console.WriteLine("|  4/ - retour -                                |");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|-----------------------------------------------|\n");
            Console.WriteLine("rentrez votre choix.");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("\nEntrez le nom du tag à créer :");
                    string? wantedNameTag = Console.ReadLine();
                    if (wantedNameTag == null) { continue; }
                    Console.WriteLine("\nEntrez la couleur du tag à créer :");
                    string? wantedColorTag = Console.ReadLine();
                    if (wantedColorTag == null) { continue; }
                    if (!CouleurValide(wantedColorTag))
                    { 
                        Console.WriteLine("\nEntrez une couleur valide.\n");
                        break;
                    }
                    try
                    {
                        u.CreateTag(wantedNameTag ,wantedColorTag);
                    }
                    catch (NotAllowedException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "2":
                    Tags tagtoedit;
                    Console.WriteLine("\nEntrez le nom du tag à modifier :");
                    string? wantedNameTag2 = Console.ReadLine();
                    if (wantedNameTag2 == null) { continue; }
                    try
                    {
                        tagtoedit = u.GetTagByName(wantedNameTag2);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    Console.WriteLine("Voulez vous modifier le nom du tag ? (o/N)");
                    if (Choix_DefaultNon())
                    {
                        Console.WriteLine("\nEntrez le nouveau nom du tag :");
                        string? wantedNewNameTag = Console.ReadLine();
                        if (wantedNewNameTag == null) { continue; }
                        try
                        {
                            u.EditTagName(tagtoedit, wantedNewNameTag);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        catch(AlreadyUsedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("Le nom du tag a été modifié avec succès.");
                    }
                    Console.WriteLine("Voulez vous modifier la couleur du tag ? (o/N)");
                    if (Choix_DefaultNon())
                    {
                        Console.WriteLine("\nEntrez la nouvelle couleur du tag :");
                        string? wantedNewColorTag = Console.ReadLine();
                        if (wantedNewColorTag == null) { continue; }
                        if (!CouleurValide(wantedNewColorTag))
                        {
                            Console.WriteLine("\nEntrez une couleur valide.\n");
                            break;
                        }
                        try
                        {
                            u.EditTagColor(tagtoedit, wantedNewColorTag);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("La couleur du tag a été modifiée avec succès.");
                    }
                    break;
                case "3":
                    Tags tagToDelete;
                    Console.WriteLine("\nEntrez le nom du tag à supprimer :");
                    string? wantedNameTag3 = Console.ReadLine();
                    if (wantedNameTag3 == null) { continue; }
                    try
                    {
                        tagToDelete = u.GetTagByName(wantedNameTag3);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    try
                    {
                        u.DeleteTag(tagToDelete);
                    }
                    catch (NotFoundException ex)
                    {
                        Console.WriteLine(ex.Message);
                        break;
                    }
                    break;
                case "4":
                    tags = false;
                    break;
                default:
                    Console.WriteLine("\nEntrez un choix valide.\n");
                    break;
            }
        }

        while (para)
        {
            Console.WriteLine("\n|--------------------------------------|");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|           menu - paramêtre           |");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|--------------------------------------|--------|");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|  1/ - modifier compte -                       |");
            Console.WriteLine("|  2/ - thèmes -                                |");
            Console.WriteLine("|  3/ - retour -                                |");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|-----------------------------------------------|\n");
            Console.WriteLine("rentrez votre choix.");
            switch (Console.ReadLine())
            {
                case "1":
                    paraCompte = true;
                    break;
                case "2":
                    theme = true;
                    break;
                case "3":
                    para = false;
                    break;
                default:
                    Console.WriteLine("\nEntrez un choix valide.\n");
                    break;
            }
            while (paraCompte)
            {
                Console.WriteLine("\n|--------------------------------------|");
                Console.WriteLine("|                                      |");
                Console.WriteLine("|           paramêtre - compte         |");
                Console.WriteLine("|                                      |");
                Console.WriteLine("|--------------------------------------|--------|");
                Console.WriteLine("|                                               |");
                Console.WriteLine("|  1/ - modifier pseudo -                       |");
                Console.WriteLine("|  2/ - modifier mot de passe -                 |");
                Console.WriteLine("|  3/ - modifier l'email                        |");
                Console.WriteLine("|  4/ - modifier l'image de profil              |");
                Console.WriteLine("|  5/ - supprimer le compte -                   |");
                Console.WriteLine("|  6/ - retour -                                |");
                Console.WriteLine("|                                               |");
                Console.WriteLine("|-----------------------------------------------|\n");
                Console.WriteLine("rentrez votre choix.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("\nEntrez votre nouveau pseudo :");
                        string? wantedNewPseudo = Console.ReadLine();
                        if (wantedNewPseudo == null) { continue; }
                        try
                        {
                            db.ChangeUsername(u,wantedNewPseudo);
                        }
                        catch (AlreadyUsedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("Votre pseudo a été modifié avec succès.");
                        break;
                    case "2":
                        Console.WriteLine("\nEntrez votre nouveau mot de passe :");
                        string? wantedNewPassword = Console.ReadLine();
                        if (wantedNewPassword == null) { continue; }
                        try 
                        {
                            u.ChangePassword(wantedNewPassword);
                        }
                        catch(NotAllowedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("Votre mot de passe a été modifié avec succès.");
                        break;
                    case "3":
                        Console.WriteLine("\nEntrez votre nouvelle adresse email :");
                        string? wantedNewEmail = Console.ReadLine();
                        if (wantedNewEmail == null) { continue; }
                        if (!IsValidEmail(wantedNewEmail))
                        {
                            Console.WriteLine("\nEntrez une adresse email valide.\n");
                            break;
                        }
                        try
                        {
                            u.ChangeEmail(wantedNewEmail);
                        }
                        catch (AlreadyUsedException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        break;
                    case "4":
                        Console.WriteLine("\nEntrez le chemin de votre nouvelle image de profil :");
                        string? wantedNewProfilePicture = Console.ReadLine();
                        if (wantedNewProfilePicture == null) { continue; }
                        if (!File.Exists(wantedNewProfilePicture))
                        {
                            Console.WriteLine("\nEntrez un chemin valide.\n");
                            break;
                        }
                        try
                        {
                            u.ChangeProfilePicture(wantedNewProfilePicture);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        catch(AlreadyExistException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        break;
                    case "5":
                        Console.WriteLine("\nÊtes-vous sûr de vouloir supprimer votre compte ? (o/N)");
                        if (Choix_DefaultNon())
                        {
                            try
                            {
                                db.RemoveUser(u);
                            }
                            catch (NotFoundException ex)
                            {
                                Console.WriteLine(ex.Message);
                                break;
                            }
                            Console.WriteLine("Votre compte a été supprimé avec succès.");
                            paraCompte = false;
                            para = false;
                            u = uvide;
                        }

                        break;
                    case "6":
                        paraCompte = false;
                        break;
                    default:
                        Console.WriteLine("\nEntrez un choix valide.\n");
                        break;
                }
            }
            while (theme)
            {
                Console.WriteLine("\n|--------------------------------------|");
                Console.WriteLine("|                                      |");
                Console.WriteLine("|           paramêtre - thèmes         |");
                Console.WriteLine("|                                      |");
                Console.WriteLine("|--------------------------------------|--------|");
                Console.WriteLine("|                                               |");
                Console.WriteLine("|  1/ - choisir un thème -                      |");
                Console.WriteLine("|  2/ - créer un thème -                        |");
                Console.WriteLine("|  3/ - supprimer un thème -                    |");
                Console.WriteLine("|  4/ - modifier un thème -                     |");
                Console.WriteLine("|  5/ - retour -                                |");
                Console.WriteLine("|                                               |");
                Console.WriteLine("|-----------------------------------------------|\n");
                Console.WriteLine("rentrez votre choix.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Theme ThemetoSelect;
                        Console.WriteLine("\nEntrez le nom du thème :");
                        string? wantedTheme = Console.ReadLine();
                        if (wantedTheme == null) { continue; }
                        try
                        {
                            ThemetoSelect = db.GetTheme(wantedTheme);
                        }
                        catch (NotFoundException)
                        {
                            try
                            {
                                ThemetoSelect = u.GetTheme(wantedTheme);
                            }
                            catch (NotFoundException ex2)
                            {
                                Console.WriteLine(ex2.Message);
                                break;
                            }
                        }
                        try
                        {
                            u.ChangeTheme(ThemetoSelect);
                        }
                        catch (AlreadyExistException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        break;
                    case "2":
                        List<string> couleurs;
                        Console.WriteLine("\nEntrez le nom du thème :");
                        string? wantedNewTheme = Console.ReadLine();
                        if (wantedNewTheme == null) { continue; }
                        if (!db.VerifThemeNameNotTaken(wantedNewTheme))
                        {
                            Console.WriteLine("\nCe nom de thème est déjà pris.\n");
                            break;
                        }
                        else
                        {
                            couleurs = Choix_CouleursToTheme();
                        }
                        try
                        {
                            u.AddTheme(new Theme(wantedNewTheme, couleurs));
                        }catch(AlreadyExistException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        break;
                    case "3":
                        Theme ThemeToDelete;
                        Console.WriteLine("\nEntrez le nom du thème :");
                        string? wantedThemeToDelete = Console.ReadLine();
                        if (wantedThemeToDelete == null) { continue; }
                        try
                        {
                            ThemeToDelete = u.GetTheme(wantedThemeToDelete);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        try
                        {
                            u.RemoveTheme(ThemeToDelete);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        break;
                    case "4":
                        Theme ThemeToModify;
                        Console.WriteLine("\nEntrez le nom du thème :");
                        string? wantedThemeToModify = Console.ReadLine();
                        if (wantedThemeToModify == null) { continue; }
                        try
                        {
                            ThemeToModify = u.GetTheme(wantedThemeToModify);
                        }
                        catch (NotFoundException ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                        Console.WriteLine("\nVoules Vous changer le nom du theme : (o/N)");
                        if (Choix_DefaultNon())
                        {
                            Console.WriteLine("\nEntrez le nouveau nom du thème :");
                            string? wantedNewThemeName = Console.ReadLine();
                            if (wantedNewThemeName == null) { continue; }
                            if(!db.VerifThemeNameNotTaken(wantedNewThemeName))
                            {
                                Console.WriteLine("\nCe nom de thème est déjà pris.\n");
                                break;
                            }
                            try
                            {
                                u.ChangeThemeName(ThemeToModify, wantedNewThemeName);
                            }
                            catch(AlreadyExistException ex)
                            {
                                Console.WriteLine(ex.Message);
                                break;
                            }
                        }
                        Console.WriteLine("\nVoules Vous changer les couleurs du theme : (o/N)");
                        if (Choix_DefaultNon())
                        {
                            List<string> couleursToChange;
                            couleursToChange = Choix_CouleursToTheme();
                            try
                            {
                                u.ChangeThemeColors(ThemeToModify, couleursToChange);
                            }
                            catch (NotFoundException ex)
                            {
                                Console.WriteLine(ex.Message);
                                break;
                            }
                            catch (AlreadyExistException ex)
                            {
                                Console.WriteLine(ex.Message);
                                break;
                            }
                        }
                        break;
                    case "5":
                        theme = false;
                        break;
                    default:
                        Console.WriteLine("\nEntrez un choix valide.\n");
                        break;
                }
            }
        }
    }
}

managerSave.SaveDatabaseData(db);
managerSave.SaveDefaultData(db);
