using Microsoft.Maui.Controls;
using Biblioteque_de_Class;
using Notus_Persistance;
using NHibernate.Mapping;
using System.Collections.ObjectModel;

namespace notus;

public partial class ProfilPage : ContentPage
{

    public User SelectedUser = (Application.Current as App).SelectedUser;
    private string username;
    public string Username
    {
        get => username;
        set
        {
            username = value;
            OnPropertyChanged(nameof(Username));
            (Application.Current as App).SelectedUser.Username = username;
        }
    }
    private string password;
    public string Password
    {
        get => password;
        set
        {
            password = value;
            OnPropertyChanged(nameof(Password));
            (Application.Current as App).SelectedUser.Password = password;
        }
    }
    private string profilPicture;
    public string ProfilPicture
    {
        get => profilPicture;
        set
        {
            profilPicture = value;
            OnPropertyChanged(nameof(ProfilPicture));
            (Application.Current as App).SelectedUser.Picture = profilPicture;
        }
    }

    public ProfilPage()
	{
        InitializeComponent();
        Username = SelectedUser.Username;
        Password = SelectedUser.Password;
        ProfilPicture = SelectedUser.Picture;
        BindingContext = this;


     }
    private void Back_Clicked(object sender, EventArgs e)
    {
        Navigation.PopAsync();
    }

    private void Disconnect_Clicked(object sender, EventArgs e)
    {
        (Application.Current as App).SelectedUser.IsConnected = false;
        Navigation.PopAsync();
        Navigation.PopAsync();
        Navigation.PopAsync();
        (Application.Current as App).SelectedUser = null;
    }

    private void AddTag_Clicked(object sender, EventArgs e)
    {
        // Handle add tag action
        // Show color picker and name input dialog
        // Create a new Tag object and add it to the Tags collection
    }

    private void Modify_Clicked(object sender, EventArgs e)
    {
        // Handle modify action for the tag
    }

    private void Delete_Clicked(object sender, EventArgs e)
    {
        // Handle delete action for the tag
    }

    private void Modify_Profil(object sender, EventArgs e)
    {
        if (ProfilZone.IsVisible == true) { ProfilZone.IsVisible = false; return; }
        ThemeZone.IsVisible = false;
        ProfilZone.IsVisible = true;
    }

    private void Modify_Theme(object sender, EventArgs e)
    {
        if (ThemeZone.IsVisible == true) { ThemeZone.IsVisible = false; return;}
        ProfilZone.IsVisible = false;
        ThemeZone.IsVisible = true;
    }
}