﻿using Biblioteque_de_Class;

namespace notus;

public partial class InscrPage : ContentPage
{
    Database datab = (Application.Current as App).db;
    public string Username { get; set; }
    public string Password { get; set; }
    public string RePassword { get; set; }
    public InscrPage()
    {
        InitializeComponent();
        BindingContext = this;
    }

    private async void Inscription_Clicked(object sender, EventArgs e)
    {
        if (Password.Length < 8) { shortPassword.IsVisible = true; return; }
        if (Password != RePassword)
        {
            await DisplayAlert("Erreur", "Les mots de passe ne sont pas identiques", "OK");
            return;
        }
        else if (Username == null || Password == null)
        {
            await DisplayAlert("Erreur", "Veuillez remplir tous les champs", "OK");
            return;
        }
        try 
        {
            datab.AddUser(new User(Username,"",Password));
        }
        catch (AlreadyUsedException)
        {
            userUser.IsVisible = true;
            return;
        }
        (Application.Current as App).SelectedUser = datab.GetUser(Username);
        await Navigation.PushAsync(new RecherPage());
    }

    private void Back_Clicked(object sender, EventArgs e)
    {
        Navigation.PopAsync();
    }
}