﻿
using Biblioteque_de_Class;
using Notus_Persistance;

namespace notus;

public partial class App : Application
{
    public PersistenceManager Mgr { get; private set; } = new PersistenceManager(new Stub());
    public Database db = new Database();

    public App()
	{
		InitializeComponent();
		db = Mgr.LoadDatabaseData();
        BindingContext = db;
        MainPage = new AppShell();
	}
    protected override Window CreateWindow(IActivationState activationState)
    {
        Window window = base.CreateWindow(activationState);

        // Set minimum height and width
        window.MinimumHeight = 670;
        window.MinimumWidth = 1200;

        return window;
    }

    private User selecUser;
    public User SelectedUser
    {
        get
        {
            return selecUser;
        }
        set
        {
            selecUser = value;
            OnPropertyChanged(nameof(SelectedUser));
        }
    }

    private Note selecNote;
    public Note SelectedNote
    {
        get
        {
            return selecNote;
        }
        set
        {
            selecNote = value;
            OnPropertyChanged(nameof(SelectedNote));
        }
    }


}
