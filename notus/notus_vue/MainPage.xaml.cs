﻿namespace notus;

public partial class MainPage : ContentPage{
	public MainPage(){
		InitializeComponent();
	}

    private async void Connec_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new ConnecPage());
    }

    private async void Inscr_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new InscrPage());
    }

}

