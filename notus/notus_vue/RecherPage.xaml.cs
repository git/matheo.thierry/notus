using Microsoft.Maui.Controls;
using Biblioteque_de_Class;
using Notus_Persistance;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.Maui;
using System.Globalization;
using System.Drawing;

namespace notus
{
    public partial class RecherPage : ContentPage, INotifyPropertyChanged
    {
        private ObservableCollection<Note> noteList;
        public ObservableCollection<Note> NoteList
        {
            get { return noteList; }
            set
            {
                noteList = value;
                OnPropertyChanged(nameof(NoteList));
            }
        }

        private ObservableCollection<Note> allNotes; // Store all notes initially
        private Database db = (Application.Current as App).db;
        public User SelectedUser = (Application.Current as App).SelectedUser;
        public Note SelectedNote
        {
            get => (Application.Current as App).SelectedNote;
            set
            {
                if ((Application.Current as App).SelectedNote != value)
                {
                    (Application.Current as App).SelectedNote = value;
                    OnPropertyChanged(nameof(SelectedNote));
                    NoteList = new ObservableCollection<Note>(SelectedUser.NoteList);
                    allNotes = new ObservableCollection<Note>(NoteList);
                    Name = SelectedNote.Name;
                    Text = SelectedNote.Text;
                    LogoPath = SelectedNote.LogoPath;
                }
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged(nameof(Name));
                    if (SelectedNote != null)
                    {
                        try
                        {
                            SelectedNote.ChangeName(SelectedUser, value);
                        }
                        catch (NotAllowedException)
                        {
                            Name = SelectedNote.Name;
                        }
                    }
                }
            }
        }
        private string text;
        public string Text
        {
            get { return text; }
            set
            {
                if (text != value)
                {
                    text = value;
                    OnPropertyChanged(nameof(Text));
                    if (SelectedNote != null)
                    {
                        try
                        {
                            SelectedNote.VerifyPrivilege(SelectedUser);
                        }
                        catch (NotAllowedException)
                        {
                            Text = SelectedNote.Text;
                            return;
                        }
                        SelectedNote.Text = value;
                    }
                }
            }
        }
        private string logoPath;
        public string LogoPath
        {
            get { return logoPath; }
            set
            {
                if (logoPath != value)
                {
                    logoPath = value;
                    OnPropertyChanged(nameof(LogoPath));
                    if (SelectedNote != null)
                    {
                        try
                        {
                            SelectedNote.ChangeLogo(SelectedUser, value);
                        }
                        catch (NotAllowedException)
                        {
                            LogoPath = SelectedNote.LogoPath;
                        }
                    }
                }
            }
        }
        public string SearchNoteText { get; set; }
        public string ProfilPicture { get; set; }
        public RecherPage()
        {
            InitializeComponent();
            allNotes = new ObservableCollection<Note>(SelectedUser.NoteList);
            NoteList = allNotes;

            SelectedNote = SelectedUser.NoteList
                .OrderByDescending(note => note.ModificationDate)
                .FirstOrDefault();

            (Application.Current as App).SelectedNote = SelectedNote;
            Name = SelectedNote.Name;
            Text = SelectedNote.Text;
            LogoPath = SelectedNote.LogoPath;
            ProfilPicture = SelectedUser.Picture;
            BindingContext = this;
        }

        private void CreateNote_Clicked(object sender, EventArgs e)
        {
            SelectedUser.CreateNote("", "");
            SelectedNote = SelectedUser.NoteList[SelectedUser.NoteList.Count - 1];
        }

        private async void Profil_Clicked(object sender, EventArgs e)
        {
            (Application.Current as App).SelectedUser = SelectedUser;
            await Navigation.PushAsync(new ProfilPage());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        void OnEditorTextChanged(object sender, TextChangedEventArgs e)
        {
            string newText = e.NewTextValue;
            if (SelectedNote != null)
            {
                SelectedNote.Text = newText;
                (Application.Current as App).SelectedNote = SelectedNote;
            }
            Text = SelectedNote.Text;
        }

        void OnEditorCompleted(object sender, EventArgs e)
        {
            if (SelectedNote != null)
            {
                SelectedNote.Text = ((Editor)sender).Text;
                (Application.Current as App).SelectedNote = SelectedNote;
            }
        }

        private void NoteListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Note selectedNote)
            {
                SelectedNote = selectedNote;
                (Application.Current as App).SelectedNote = SelectedNote;
                Name = SelectedNote.Name;
                Text = SelectedNote.Text;
                LogoPath = SelectedNote.LogoPath;
            }
        }

        private void AddUser_Clicked(object sender, EventArgs e)
        {
            if (AddUser_Zone.IsVisible)
                AddUser_Zone.IsVisible = false;
            else
                AddUser_Zone.IsVisible = true;
        }

        private void Valid_Clicked(object sender, EventArgs e)
        {
            AddUser_Zone.IsVisible = false;
        }

        private void AddUserCheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
        }

        public void Delete_Note(object sender, EventArgs e)
        {
            if (SelectedNote != null)
            {
                SelectedUser.NoteList.Remove(SelectedNote);
                if (SelectedUser.NoteList.Count == 0)
                {
                    SelectedUser.CreateNote("","");
                }
                SelectedNote = SelectedUser.NoteList.FirstOrDefault(); // Select the first note in the list (or null if empty)
            }
        }

        private int windowHeight;
        public int WindowHeight
        {
            get { return windowHeight; }
            set
            {
                if (windowHeight != value)
                {
                    windowHeight = value - 300;
                    OnPropertyChanged(nameof(WindowHeight));
                    UpdateScrollViewHeight();
                }
            }
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (WindowHeight != (int)height)
            {
                WindowHeight = (int)height;
                UpdateScrollViewHeight();
            }
        }

        private void UpdateScrollViewHeight()
        {
            double availableHeight = WindowHeight;
            TheScrollView.MaximumHeightRequest = availableHeight;
            UserListScrollView.MinimumHeightRequest = availableHeight;
            UserListScrollView.MaximumHeightRequest = availableHeight;
        }

        private bool isFavoritesFilterChecked = false;
        private List<Tags> selectedTags = new List<Tags>();
        private DateOnly startDate;
        private DateOnly endDate;
        private bool isfavorite;
        public bool IsFavorite 
        {
            get => isfavorite;
            set
            {
                if (IsFavorite != value)
                {
                      IsFavorite = value;
                    OnPropertyChanged(nameof(IsFavorite));
                }
            } 
        }

        void OnSearchNoteTextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = e.NewTextValue;
            if (string.IsNullOrEmpty(searchText))
            {
                ApplyFilters();
            }
            else
            {
                var filteredNotes = NoteList.Where(note => note.Name.Contains(searchText));
                NoteList = new ObservableCollection<Note>(filteredNotes);
            }
        }

        private void FavoritesFilter_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            isFavoritesFilterChecked = e.Value;
            ApplyFilters();
        }

        private void TagsFilter_Clicked(object sender, EventArgs e)
        {
            ApplyFilters();
        }

        private void DatesFilter_Clicked(object sender, EventArgs e)
        {
            ApplyFilters();
        }

        private void ApplyFilters()
        {

            var filteredNotes = allNotes;

            if (isFavoritesFilterChecked)
            {
                var FavfilteredNotes = allNotes.Where(note => SelectedUser.FavList.Contains(note));
                NoteList = new ObservableCollection<Note>(FavfilteredNotes);
                filteredNotes = NoteList;
            }

            if (selectedTags.Count > 0)
            {
                filteredNotes = (ObservableCollection<Note>)filteredNotes.Where(note => SelectedUser.NoteTagged[note].Any(tag => selectedTags.Contains(tag)));
            }

            if (startDate != default && endDate != default)
            {
                filteredNotes = (ObservableCollection<Note>)filteredNotes.Where(note => note.CreationDate >= startDate && note.CreationDate <= endDate || note.ModificationDate >= startDate && note.ModificationDate <= endDate);
            }

            foreach (var note in filteredNotes)
            {
                note.IsFavorite = SelectedUser.FavList.Contains(note);
                IsFavorite = note.IsFavorite;
            }

            NoteList = new ObservableCollection<Note>(filteredNotes);
        }

        private void FavoriteCheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            Note note = checkBox.BindingContext as Note;

            if (note != null)
            {
                if (e.Value)
                {
                    if(note.IsFavorite == false)
                        SelectedUser.AddFavorite(note);
                        (Application.Current as App).SelectedUser = SelectedUser;
                        allNotes = new ObservableCollection<Note>(SelectedUser.NoteList);
                        
                }
                else
                {
                    if (note.IsFavorite == true)
                        SelectedUser.RemoveFavorite(note);
                        (Application.Current as App).SelectedUser = SelectedUser;
                        allNotes = new ObservableCollection<Note>(SelectedUser.NoteList);
                }
            }
        }
    }
}