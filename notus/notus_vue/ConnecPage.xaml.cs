using Biblioteque_de_Class;
using Windows.UI.Core;

namespace notus;
public partial class ConnecPage : ContentPage
{
    User selectUser = (Application.Current as App).SelectedUser;
    Database db = (Application.Current as App).db;
    public string Username { get; set; }
    public string Password { get; set; }

    public ConnecPage()
    {
        InitializeComponent();
        BindingContext = this;
    }

    private void Connec_Clicked(object sender, EventArgs e)
    {
        try
        {
            selectUser = db.GetUser(Username);
        }
        catch (NotFoundException)
        {
            DisplayAlert("Erreur", "Cet utilisateur n'existe pas" , "OK");
            return;
        }
        if (Database.ComparePassword(selectUser, HashCodeModel.GetSHA256Hash(Password).ToString()))
        {
            selectUser.IsConnected = true;
            (Application.Current as App).SelectedUser = selectUser;
            Navigation.PushAsync(new RecherPage());
        }
        else
        {
            DisplayAlert("Erreur", "Le mot de passe est incorrect", "OK");
            return;
        }
    }

    private void Back_Clicked(object sender, EventArgs e)
    {
          Navigation.PopAsync();
    }
}