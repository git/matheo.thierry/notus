using Biblioteque_de_Class;
namespace UnitTests_Model
{
    [TestFixture]
    public class Database_Tests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            database.UserList.Add(new User("John", "john@example.com", "password123"));
            database.UserList.Add(new User("Jane", "jane@example.com", "choco"));
            database.UserList.Add(new User("Alice", "alice@example.com", "choco"));
            database.DefaultLogoList.Add(new Logo("Logo1", "link1"));
            database.DefaultLogoList.Add(new Logo("Logo2", "link2"));
            database.DefaultLogoList.Add(new Logo("Logo3", "link3"));
        }

        // SearchUser tests

        [Test]
        public void SearchUser_UserDoesNotExist_ThrowsException()
        {
            string searchName = "Bob";
            Assert.Throws<NotFoundException>(() => database.SearchUser(searchName));
        }

        [Test]
        public void SearchUser_CaseInsensitiveSearch_ReturnsMatchingUsers()
        {
            string searchName = "Alice";
            User searchedUser = database.SearchUser(searchName);
            Assert.That(searchedUser.Username, Is.EqualTo("Alice"));
        }

        // GetLogoLink tests
        [Test]
        public void GetLogoLink_LogoExists_ReturnsLogoLink()
        {
            Assert.That(database.GetLogoLink("Logo2"), Is.EqualTo("link2"));
        }

        [Test]
        public void GetLogoLink_LogoDoesNotExist_ThrowsException()
        {
            string logoName = "Logo4";
            Assert.Throws<NotFoundException>(() => database.GetLogoLink(logoName));
        }

        // GetUser tests
        [Test]
        public void GetUser_UserExists_ReturnsUser()
        {
            string userName = "Alice";
            User user = database.GetUser(userName);
            Assert.IsNotNull(user);
            Assert.That(user.Username, Is.EqualTo(userName));
        }

        [Test]
        public void GetUser_UserDoesNotExist_ThrowsException()
        {
            string userName = "Eve";
            Assert.Throws<NotFoundException>(() => database.GetUser(userName));
        }

        // ComparePassword tests
        [Test]
        public void ComparePassword_CorrectPassword_ReturnsTrue()
        {
            User user = database.UserList[0];
            string password = "password123";
            bool result = Database.ComparePassword(user, password);
            Assert.That(result, Is.True);
        }

        [Test]
        public void ComparePassword_IncorrectPassword_ReturnsFalse()
        {
            User user = database.UserList[0];
            string password = "incorrectPassword";
            bool result = Database.ComparePassword(user, password);
            Assert.That(result, Is.False);
        }

        // FindEmail tests
        [Test]
        public void FindEmail_ExistingEmail_ReturnsTrue()
        {
            string email = "john@example.com";
            bool result = database.FindEmail(email);
            Assert.IsTrue(result);
        }

        [Test]
        public void FindEmail_NonExistingEmail_ReturnsFalse()
        {
            string email = "olivedecarglass@example.com";
            bool result = database.FindEmail(email);
            Assert.IsFalse(result);
        }

        // AddUser tests
        [Test]
        public void AddUser_ValidUser_AddsUserToList()
        {
            User user = new User("Bob", "bob@example.com", "password123");
            database.AddUser(user);
            Assert.That(database.UserList, Contains.Item(user));
        }

        [Test]
        public void AddUser_DuplicateUserName_ThrowsException()
        {
            User user = new User("John", "johnDae@example.com", "password123");
            Assert.Throws<AlreadyUsedException>(() => database.AddUser(user));
        }

        [Test]
        public void AddUser_DuplicateUserEmail_ThrowsException()
        {
            User user = new User("Bob", "john@example.com", "password123");
            Assert.Throws<AlreadyUsedException>(() => database.AddUser(user));
        }

        // removeUser tests
        [Test]
        public void RemoveUser_ExistingUser_RemovesUserFromList()
        {
            User user = database.UserList[0];
            database.RemoveUser(user);
            Assert.That(database.UserList, !Contains.Item(user));
        }

        [Test]
        public void RemoveUser_NotExistingUser_ThrowsException()
        {
            User user = new User("Bob", "bob@example.com", "password123");
            Assert.Throws<NotFoundException>(() => database.RemoveUser(user));
        }

        // AddTheme tests
        [Test]
        public void AddTheme_ValidTheme_AddsThemeToList()
        {
            Theme theme = new Theme("Theme1", ",,,".Split().ToList());
            database.AddTheme(theme);
            Assert.That(database.ThemeList, Contains.Item(theme));
        }

        [Test]
        public void AddTheme_DuplicateTheme_ThrowsException()
        {
            Theme theme = new Theme("Theme1", ",,,".Split().ToList());
            database.ThemeList.Add(theme);
            Assert.Throws<AlreadyExistException>(() => database.AddTheme(theme));
        }

        // GetTheme tests
        [Test]
        public void GetTheme_ExistingTheme_ReturnsTheme()
        {
            Theme expectedTheme = new Theme("Theme1", ",,,".Split().ToList());
            database.ThemeList.Add(expectedTheme);

            Theme theme = database.GetTheme("Theme1");
            Assert.IsNotNull(theme);
            Assert.That(theme, Is.EqualTo(expectedTheme));
        }

        [Test]
        public void GetTheme_NonExistingTheme_ReturnsNull()
        {
            Theme expectedTheme = new Theme("Theme1", ",,,".Split().ToList());
            database.ThemeList.Add(expectedTheme);
            Assert.Throws<NotFoundException>(() => database.GetTheme("NonExistingTheme"));
        }

        // ChangeUsername tests
        [Test]
        public void ChangeUsername_CorrectReplaceName_ChangesUsername()
        {
            User userSelected = database.UserList[0];
            string newUsername = "duberlute";

            database.ChangeUsername(userSelected, newUsername);

            User updatedUser = database.UserList.Where(u => u.Username == newUsername).First();
            Assert.IsNotNull(updatedUser);
            Assert.That(updatedUser.Username, Is.EqualTo(newUsername));
        }

        [Test]
        public void ChangeUsername_UsernameAlreadyUsed_ThrowsException()
        {
            User userNotSelected = database.UserList[2];
            string newUsername = "Jane";

            Assert.Throws<AlreadyUsedException>(() => database.ChangeUsername(userNotSelected, newUsername));
        }

        // VerifThemeNameNotTaken tests
        [Test]
        public void VerifThemeNameNotTaken_NameNotTaken_ReturnsTrue()
        {
            string themeName = "NewTheme";
            bool result = database.VerifThemeNameNotTaken(themeName);
            Assert.IsTrue(result);
        }

        [Test]
        public void VerifThemeNameNotTaken_NameAlreadyTaken_ReturnsFalse()
        {
            Theme expectedTheme = new Theme("Theme1", ",,,".Split().ToList());
            database.ThemeList.Add(expectedTheme);
            string themeName = "Theme1";
            bool result = database.VerifThemeNameNotTaken(themeName);
            Assert.IsFalse(result);
        }
    }
}
