﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests_Model
{
    public class User_Tests
    {
        private User user;

        [SetUp]
        public void Setup()
        {
            user = new User("John", "john@example.com", "password123");
            user.CreateTag("tag1","#FF0000");
            user.CreateTag("tag2", "#00FF00");
            user.CreateNote("note1", "default.png");
            user.AddTagFromNoteList(user.NoteList[0], user.TagList[0]);
            user.CreateNote("note2", "default.png");
            user.AddTheme(new Theme("white","#FFAAAA,#000000,#999999".Split(',').ToList()));
        }

        //test searchnoteByName
        [Test]
        public void User_SearchNoteByName_ValidData_ReturnsNote()
        {
            Note note = user.SearchNoteByName(user.NoteList,"note1").First();
            Assert.That(note.Name, Is.EqualTo("note1"));
        }
        [Test]
        public void User_SearchNoteByName_InvalidData_ReturnsNull()
        {
            Assert.That(user.SearchNoteByName(user.NoteList, "note3"), Is.Empty);
        }

        //test seachNoteBytag
        [Test]
        public void User_SearchNoteByTag_ValidData_ReturnsNote()
        {
            Note note = user.SearchNoteByTag(user.NoteList, "tag1").First();
            Assert.That(note.Name, Is.EqualTo("note1"));
        }
        [Test]
        public void User_SearchNoteByTag_InvalidData_ReturnsNull()
        {
            Assert.Throws<NotFoundException>(() => user.SearchNoteByTag(user.NoteList, "tag3"));
        }

        //test SearchNoteByDate
        [Test]
        public void User_SearchNoteByDate_ValidData_ReturnsNote()
        {
            Note note = user.SearchNoteByDate(user.NoteList, "note1", DateOnly.FromDateTime(DateTime.Now), DateOnly.FromDateTime(DateTime.Now)).First();
            Assert.That(note.Name, Is.EqualTo("note1"));
        }

        [Test]
        public void User_SearchNoteByDate_InvalidData_ReturnsNull()
        {
            Assert.That(user.SearchNoteByDate(user.NoteList, "note3", DateOnly.FromDateTime(DateTime.Now), DateOnly.FromDateTime(DateTime.Now)), Is.Empty);
        }

        //test GetNoteByName
        [Test]
        public void User_GetNoteByName_ValidData_ReturnsNote()
        {
            Note note = user.GetNoteByName("note1");
            Assert.That(note.Name, Is.EqualTo("note1"));
        }

        [Test]
        public void User_GetNoteByName_InvalidData_ReturnsNull()
        {
            Assert.Throws<NotFoundException>(() => user.GetNoteByName("note3"));
        }

        //test searchTagByName
        [Test]
        public void User_SearchTagByName_ValidData_ReturnsTag()
        {
            Tags tag = user.SearchTagByName(user.TagList, "tag1").First();
            Assert.That(tag.Name, Is.EqualTo("tag1"));
        }
        [Test]
        public void User_SearchTagByName_InvalidData_ReturnsNull()
        {
            Assert.That(user.SearchTagByName(user.TagList, "tag3"), Is.Empty);
        }

        //test AddFavorite
        [Test]
        public void User_AddFavorite_ValidData_AddNote()
        {
            user.AddFavorite(user.NoteList[0]);
            Assert.That(user.FavList.Count, Is.EqualTo(1));
        }
        [Test]
        public void User_AddFavorite_InvalidData_ThrowException()
        {
            user.AddFavorite(user.NoteList[0]);
            Assert.Throws<AlreadyExistException>(() => user.AddFavorite(user.NoteList[0]));
        }

        //test RemoveFavorite

        [Test]
        public void User_RemoveFavorite_ValidData_RemoveNote()
        {
            user.AddFavorite(user.NoteList[0]);
            user.RemoveFavorite(user.NoteList[0]);
            Assert.That(user.FavList, Is.Empty);
        }
        [Test]
        public void User_RemoveFavorite_InvalidData_ThrowException()
        {
            Assert.Throws<NotFoundException>(() => user.RemoveFavorite(user.NoteList[0]));
        }

        //test createNote
        [Test]
        public void User_CreateNote_ValidData_AddNote()
        {
            user.CreateNote("note3", "default.png");
            Assert.That(user.NoteList.Count, Is.EqualTo(3));
        }

        [Test]
        public void User_CreateNote_InvalidData_ThrowException()
        {
            user.CreateNote("note3", "default.png");
            Assert.Throws<AlreadyExistException>(() => user.CreateNote("note3", "default.png"));
        }
        [Test]
        public void User_CreateNote_ValidData_secondNote_AddNote()
        {
            User user2 = new User("John", "rien", "choco");
            user2.CreateNote("note3", "default.png");
            user2.CreateNote("note4", "default.png");
            Assert.That(user2.NoteList.Count, Is.EqualTo(2));
            Assert.That(user2.NoteList[1].id, Is.EqualTo(1));
        }

        //test DeleteNote
        [Test]
        public void User_DeleteNote_ValidData_RemoveNote()
        {
            Note note = user.NoteList[0];
            user.DeleteNote(note);
            Assert.That(user.NoteList.Count, Is.EqualTo(1));
        }

        [Test]
        public void User_DeleteNote_InvalidData_ThrowException()
        {
            Note note = user.NoteList[0];
            user.DeleteNote(note);
            Assert.Throws<NotFoundException>(() => user.DeleteNote(note));
        }

        //test CreateTag
        [Test]
        public void User_CreateTag_ValidData_AddTag()
        {
            user.CreateTag("tag3", "#FF0000");
            Assert.That(user.TagList.Count, Is.EqualTo(3));
        }

        [Test]
        public void User_CreateTag_InvalidData_ThrowException()
        {
            user.CreateTag("tag3", "#FF0000");
            Assert.Throws<AlreadyExistException>(() => user.CreateTag("tag2", "#FF0000"));
        }

        //test DeleteTag
        [Test]
        public void User_DeleteTag_ValidData_RemoveTag()
        {
            Tags tag = user.TagList[0];
            user.DeleteTag(tag);
            Assert.That(user.TagList.Count, Is.EqualTo(1));
        }

        [Test]
        public void User_DeleteTag_InvalidData_ThrowException()
        {
            Tags tag = user.TagList[0];
            user.DeleteTag(tag);
            Assert.Throws<NotFoundException>(() => user.DeleteTag(tag));
        }

        //test EditTagName
        [Test]
        public void User_EditTagName_ValidData_EditTag()
        {
            user.EditTagName(user.TagList[0], "tag3");
            Assert.That(user.TagList[0].Name, Is.EqualTo("tag3"));
        }

        [Test]
        public void User_EditTagName_InvalidData_ThrowException()
        {
            Tags tags = new Tags("tag2", "#FF0000");
            Assert.Throws<NotFoundException>(() => user.EditTagName(tags, "tag1"));
        }

        [Test]
        public void User_EditTagName_InvalidData2_ThrowException()
        {
            Assert.Throws<AlreadyExistException>(() => user.EditTagName(user.TagList[0], "tag1"));
        }

        //test EditTagColor
        [Test]
        public void User_EditTagColor_ValidData_EditTag()
        {
            user.EditTagColor(user.TagList[0], "#FF0000");
            Assert.That(user.TagList[0].Color, Is.EqualTo("#FF0000"));
        }

        [Test]
        public void User_EditTagColor_InvalidData_ThrowException()
        {
            Tags tags = new Tags("tag2", "#FF0000");
            Assert.Throws<NotFoundException>(() => user.EditTagColor(tags, "#000000"));
        }

        //Test AddTagFromNoteList
        [Test]
        public void User_AddTagFromNoteList_ValidData_AddTag()
        {
            user.AddTagFromNoteList(user.NoteList[0], user.TagList[1]);
            Assert.That(user.NoteTagged[user.NoteList[0]].Count, Is.EqualTo(2));
        }

        [Test]
        public void User_AddTagFromNoteList_InvalidData_ThrowException()
        {
            Tags tags = new Tags("tag2", "#FF0000");
            Assert.Throws<NotFoundException>(() => user.AddTagFromNoteList(user.NoteList[0], tags));
        }


        [Test]
        public void User_AddTagFromNoteList_InvalidData2_ThrowException()
        {
            User uvide = new User("", "", "");
            Note note = new Note(4,"rien", "default.png", uvide);
            Assert.Throws<NotFoundException>(() => user.AddTagFromNoteList(note, user.TagList[0]));
        }

        //Test RemoveTagFromNoteList
        [Test]
        public void User_RemoveTagFromNoteList_ValidData_RemoveTag()
        {
            user.RemoveTagFromNoteList(user.NoteList[0], user.TagList[0]);
            Assert.That(user.NoteTagged[user.NoteList[0]], Is.Empty);
        }

        [Test]
        public void User_RemoveTagFromNoteList_InvalidData_ThrowException()
        {
            Tags tags = new Tags("tag2", "#FF0000");
            Assert.Throws<NotFoundException>(() => user.RemoveTagFromNoteList(user.NoteList[0], tags));
        }

        [Test]
        public void User_RemoveTagFromNoteList_InvalidData2_ThrowException()
        {
            User uvide = new User("", "", "");
            Note note = new Note(4, "rien", "default.png", uvide);
            Assert.Throws<NotFoundException>(() => user.RemoveTagFromNoteList(note, user.TagList[0]));
        }

        //test SetNoteName
        [Test]
        public void User_SetNoteName_ValidData_EditNote()
        {
            user.SetNoteName(user.NoteList[0], "note3");
            Assert.That(user.NoteList[0].Name, Is.EqualTo("note3"));
        }

        [Test]
        public void User_SetNoteName_InvalidData_ThrowException()
        {
            User uvide = new User("", "", "");
            Note note = new Note(4, "rien", "default.png", uvide);
            Assert.Throws<NotFoundException>(() => user.SetNoteName(note, "note2"));
        }

        [Test]
        public void User_SetNoteName_InvalidData2_ThrowException()
        {
            Assert.Throws<AlreadyUsedException>(() => user.SetNoteName(user.NoteList[0], "note1"));
        }

        //test ChangePassword
        [Test]
        public void User_ChangePassword_ValidData_ChangePassword()
        {
            user.ChangePassword("chocoChoco");
            Assert.That(user.Password, Is.EqualTo(HashCodeModel.GetSHA256Hash("chocoChoco").ToString()));
        }

        [Test]
        public void User_ChangePassword_InvalidData_ThrowException()
        {
            user.ChangePassword("chocoChoco");
            Assert.Throws<AlreadyUsedException>(() => user.ChangePassword("chocoChoco"));
        }

        [Test]
        public void User_ChangePassword_InvalidData1_ThrowException()
        {
            Assert.Throws<NotAllowedException>(() => user.ChangePassword("choco"));
        }

        [Test]
        public void User_ChangePassword_InvalidData2_ThrowException()
        {
            Assert.Throws<NotAllowedException>(() => user.ChangePassword(""));
        }

        //test ChangeTheme
        [Test]
        public void User_ChangeTheme_ValidData_ChangeTheme()
        {
            user.ChangeTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.That(user.UseTheme.Name, Is.EqualTo("dark"));
        }

        [Test]
        public void User_ChangeTheme_InvalidData_ThrowException()
        {
            user.ChangeTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.Throws<AlreadyExistException>(() => user.ChangeTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList())));
        }

        //test ChangeThemeName
        [Test]
        public void User_ChangeThemeName_ValidData_ChangeThemeName()
        {
            user.ChangeThemeName(user.AddedTheme[0], "dark");
            Assert.That(user.AddedTheme[0].Name, Is.EqualTo("dark"));
        }

        [Test]
        public void User_ChangeThemeName_InvalidData_ThrowException()
        {
            user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.Throws<AlreadyExistException>(() => user.ChangeThemeName(user.AddedTheme[0], "dark"));
        }

        [Test]
        public void User_ChangeThemeName_InvalidData2_ThrowException()
        {
            Assert.Throws<AlreadyExistException>(() => user.ChangeThemeName(user.AddedTheme[0], "white"));
        }

        //test ChangeThemeColors
        [Test]
        public void User_ChangeThemeColors_ValidData_ChangeThemeColors()
        {
            // Arrange
            var theme = new Theme("blacktheme", new List<string> { "#000000", "#FF00FF", "#OOFFOO" });
            var newColors = new List<string> { "#111111", "#222222", "#333333" };

            // Act
            user.ChangeThemeColors(theme, newColors);

            // Assert
            Assert.That(theme.ColorList[0], Is.EqualTo(newColors[0]));
            Assert.That(theme.ColorList[1], Is.EqualTo(newColors[1]));
            Assert.That(theme.ColorList[2], Is.EqualTo(newColors[2]));
        }

        [Test]
        public void User_ChangeThemeColors_InvalidData_ThrowException()
        {
            // Arrange
            var theme = new Theme("blacktheme", new List<string> { "#000000", "#FF00FF", "#OOFFOO" });
            var sameColors = new List<string> { "#000000", "#FF00FF", "#OOFFOO" };

            // Act & Assert
            Assert.Throws<AlreadyExistException>(() => user.ChangeThemeColors(theme, sameColors));
        }

        [Test]
        public void User_ChangeThemeColors_InvalidData1_ThrowException()
        {
            // Arrange
            var theme = new Theme("blacktheme", new List<string> { "#000000", "#FF00FF", "#OOFFOO" });
            var differentColors = new List<string> { "#111111", "#222222", "#333333", "#444444" };

            // Act & Assert
            Assert.Throws<NotFoundException>(() => user.ChangeThemeColors(theme, differentColors));
        }

        //test AddTheme
        [Test]
        public void User_AddTheme_ValidData_AddTheme()
        {
            user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.That(user.AddedTheme[1].Name, Is.EqualTo("dark"));
        }

        [Test]
        public void User_AddTheme_InvalidData_ThrowException()
        {
            user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.Throws<AlreadyExistException>(() => user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList())));
        }

        //test RemoveTheme
        [Test]
        public void User_RemoveTheme_ValidData_RemoveTheme()
        {
            user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            user.RemoveTheme(user.AddedTheme[1]);
            Assert.That(user.AddedTheme.Count, Is.EqualTo(1));
        }

        [Test]
        public void User_RemoveTheme_InvalidData_ThrowException()
        {
            Assert.Throws<NotFoundException>(() => user.RemoveTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList())));
        }

        //test GetTheme
        [Test]
        public void User_GetTheme_ValidData_GetTheme()
        {
            user.AddTheme(new Theme("dark", "#0000FF,#FF00FF,#FFFFFF".Split(',').ToList()));
            Assert.That(user.GetTheme("dark").Name, Is.EqualTo("dark"));
        }

        [Test]
        public void User_GetTheme_InvalidData_ThrowException()
        {
            Assert.Throws<NotFoundException>(() => user.GetTheme("dark"));
        }

        //test ChangeEmail
        [Test]
        public void User_ChangeEmail_ValidData_ChangeEmail()
        {
            user.ChangeEmail("nouvelleEmail@gmail.com");
            Assert.That(user.Email, Is.EqualTo("nouvelleEmail@gmail.com"));
        }

        [Test]
        public void User_ChangeEmail_InvalidData_ThrowException()
        {
            Assert.Throws<NotAllowedException>(() => user.ChangeEmail(""));
        }

        //test ChangeProfilePicture
        [Test]
        public void User_ChangeProfilePicture_ValidData_ChangeProfilePicture()
        {
            user.ChangeProfilePicture("nouvellePhoto.png");
            Assert.That(user.Picture, Is.EqualTo("nouvellePhoto.png"));
        }

        [Test]
        public void User_ChangeProfilePicture_InvalidData_ThrowException()
        {
            Assert.Throws<NotFoundException>(() => user.ChangeProfilePicture("photosansextesion"));
        }

        [Test]
        public void User_ChangeProfilePicture_InvalidData2_ThrowException()
        {
            user.ChangeProfilePicture("");
            Assert.That(user.Picture, Is.EqualTo("default.png"));
        }
    }
}
