﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests_Model
{
    [TestFixture]
    public class Note_Tests
    {
        private User owner;
        private Note note;

        [SetUp]
        public void Setup()
        {
            owner = new User("John", "john@example.com", "password123");
            note = new Note(1, "Note 1", "logoPath", owner);
        }

        [Test]
        public void Note_Constructor_InitializesProperties()
        {
            Assert.That(note.id, Is.EqualTo(1));
            Assert.That(note.Name, Is.EqualTo("Note 1"));
            Assert.That(note.LogoPath, Is.EqualTo("logoPath"));
            Assert.That(note.Owner, Is.EqualTo(owner));
            Assert.That(note.ImageList.Count, Is.EqualTo(0));
            Assert.That(note.Collaborators.Count, Is.EqualTo(0));
            Assert.That(note.Editors.Count, Is.EqualTo(1));
            Assert.That(note.Editors[0], Is.EqualTo(owner));
        }

        [Test]
        public void Note_VerifyOwner_UserIsOwner_ReturnsTrue()
        {
            bool result = note.VerifyOwner(owner);
            Assert.IsTrue(result);
        }

        [Test]
        public void Note_VerifyOwner_UserIsNotOwner_ThrowsNotAllowedException()
        {
            User otherUser = new User("Jane", "jane@example.com", "password123");

            Assert.Throws<NotAllowedException>(() => note.VerifyOwner(otherUser));
        }

        [Test]
        public void Note_AddImage_ValidData_AddsImage()
        {
            string imageLink = "imageLink";
            List<int> position = new List<int> { 1, 2, 3 };

            note.AddImage(imageLink, position);

            Assert.That(note.ImageList.Count, Is.EqualTo(1));
            Assert.That(note.ImageList[0].Name, Is.EqualTo("1"));
            Assert.That(note.ImageList[0].ImageLink, Is.EqualTo(imageLink));
            Assert.That(note.ImageList[0].Position, Is.EqualTo(position));
        }

        [Test]
        public void Note_RemoveImage_ExistingImage_RemovesImage()
        {
            string imageLink = "imageLink";
            List<int> position = new List<int> { 1, 2, 3 };
            note.AddImage(imageLink, position);

            note.RemoveImage("1");

            Assert.That(note.ImageList.Count, Is.EqualTo(0));
        }

        [Test]
        public void Note_RemoveImage_NonExistingImage_ThrowsNotFoundException()
        {
            string imageLink = "imageLink";
            List<int> position = new List<int> { 1, 2, 3 };
            note.AddImage(imageLink, position);

            Assert.Throws<NotFoundException>(() => note.RemoveImage("2"));
        }

        [Test]
        public void Note_AddText_UserIsOwner_AddsText()
        {
            User user = owner;
            string text = "Some text";

            note.AddText(user, text);

            Assert.That(note.Text, Is.EqualTo("\n" + text));
            Assert.That(note.ModificationDate, Is.EqualTo(DateOnly.FromDateTime(DateTime.Now)));
        }

        [Test]
        public void Note_AddText_UserIsEditor_AddsText()
        {
            User user = new User("Editor", "editor@example.com", "password123");
            note.AddEditor(owner, user);
            string text = "Some text";

            note.AddText(user, text);

            Assert.That(note.Text, Is.EqualTo("\n"+text));
            Assert.That(note.ModificationDate, Is.EqualTo(DateOnly.FromDateTime(DateTime.Now)));
        }

        [Test]
        public void Note_AddText_UserIsNotOwnerOrEditor_ThrowsNotAllowedException()
        {
            User user = new User("Jane", "jane@example.com", "password123");
            string text = "Some text";

            Assert.Throws<NotAllowedException>(() => note.AddText(user, text));
        }

        [Test]
        public void Note_VerifyPrivilege_UserIsEditor_ReturnsTrue()
        {
            User editor = new User("Editor", "editor@example.com", "password123");
            note.AddEditor(owner, editor);

            bool result = note.VerifyPrivilege(editor);

            Assert.IsTrue(result);
        }

        [Test]
        public void Note_VerifyPrivilege_UserIsNotEditor_ReturnsFalse()
        {
            User user = new User("User", "user@example.com", "password123");

            bool result = note.VerifyPrivilege(user);

            Assert.IsFalse(result);
        }

        [Test]
        public void Note_AddCollaborator_UserIsOwner_AddsCollaborator()
        {
            User collaborator = new User("Collaborator", "collaborator@example.com", "password123");

            note.AddCollaborator(owner, collaborator);

            Assert.That(note.Collaborators.Count, Is.EqualTo(1));
            Assert.That(note.Collaborators[0], Is.EqualTo(collaborator));
            Assert.IsTrue(collaborator.NoteList.Contains(note));
        }

        [Test]
        public void Note_AddCollaborator_UserIsNotOwner_ThrowsNotAllowedException()
        {
            User otherUser = new User("OtherUser", "otheruser@example.com", "password123");
            User collaborator = new User("Collaborator", "collaborator@example.com", "password123");

            Assert.Throws<NotAllowedException>(() => note.AddCollaborator(otherUser, collaborator));
        }

        [Test]
        public void Note_RemoveCollaborator_UserIsOwner_RemovesCollaborator()
        {
            User collaborator = new User("Collaborator", "collaborator@example.com", "password123");
            note.AddCollaborator(owner, collaborator);

            note.RemoveCollaborator(owner, collaborator);

            Assert.That(note.Collaborators.Count, Is.EqualTo(0));
            Assert.IsFalse(collaborator.NoteList.Contains(note));
        }

        [Test]
        public void Note_RemoveCollaborator_UserIsNotOwner_ThrowsNotAllowedException()
        {
            User otherUser = new User("OtherUser", "otheruser@example.com", "password123");
            User collaborator = new User("Collaborator", "collaborator@example.com", "password123");
            note.AddCollaborator(owner, collaborator);

            Assert.Throws<NotAllowedException>(() => note.RemoveCollaborator(otherUser, collaborator));
        }

        [Test]
        public void Note_AddEditor_UserAlreadyEditor_ThrowsAlreadyExistException()
        {
            User editor = new User("Editor", "editor@example.com", "password123");
            note.AddEditor(owner, editor);

            Assert.Throws<AlreadyExistException>(() => note.AddEditor(owner, editor));
        }

        [Test]
        public void Note_AddEditor_UserIsOwner_AddsEditor()
        {
            User editor = new User("Editor", "editor@example.com", "password123");

            note.AddEditor(owner, editor);

            Assert.That(note.Editors.Count, Is.EqualTo(2));
            Assert.That(note.Editors[1], Is.EqualTo(editor));
        }

        [Test]
        public void Note_RemoveEditor_UserNotEditor_ThrowsNotFoundException()
        {
            User user = new User("User", "user@example.com", "password123");

            Assert.Throws<NotFoundException>(() => note.RemoveEditor(owner, user));
        }

        [Test]
        public void Note_RemoveEditor_UserIsOwner_RemovesEditor()
        {
            User editor = new User("Editor", "editor@example.com", "password123");
            note.AddEditor(owner, editor);

            note.RemoveEditor(owner, editor);

            Assert.That(note.Editors.Count, Is.EqualTo(1));
            Assert.IsFalse(note.Editors.Contains(editor));
        }

        [Test]
        public void Note_ChangeName_UserIsOwner_ChangesName()
        {
            User user = owner;
            string newName = "New Note Name";

            note.ChangeName(user, newName);

            Assert.That(note.Name, Is.EqualTo(newName));
        }

        [Test]
        public void Note_ChangeName_UserIsNotOwner_NameNotChanged()
        {
            User user = new User("OtherUser", "otheruser@example.com", "password123");
            string newName = "New Note Name";

            Assert.Throws<NotAllowedException>(() => note.ChangeName(user, newName));
        }

        [Test]
        public void Note_ChangeLogo_UserIsOwner_ChangesLogo()
        {
            User user = owner;
            string newLogoPath = "newLogoPath";

            note.ChangeLogo(user, newLogoPath);

            Assert.That(note.LogoPath, Is.EqualTo(newLogoPath));
        }

        [Test]
        public void Note_ChangeLogo_UserIsNotOwner_LogoNotChanged()
        {
            User user = new User("OtherUser", "otheruser@example.com", "password123");
            string newLogoPath = "newLogoPath";

            Assert.Throws<NotAllowedException>(() => note.ChangeLogo(user, newLogoPath));
        }
    }
}
