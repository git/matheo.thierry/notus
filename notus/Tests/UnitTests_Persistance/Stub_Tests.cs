﻿using Biblioteque_de_Class;
using Notus_Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests_Persistance
{
    public class Stub_Tests
    {
        PersistenceManager manager;
        Database result;

        [SetUp]
        public void Setup()
        {
            manager = new PersistenceManager(new Stub());
            result = new Database();
        }

        [Test]
        public void SaveDatabaseData_Test()
        {
            Assert.Throws<NotImplementedException>(() => manager.SaveDatabaseData(result));
        }

        [Test]
        public void LoadDatabaseData_Test()
        {
            result = manager.GetOnlyDatabaseUser();

            Assert.NotNull(result);
            Assert.NotNull(result.UserList);
            Assert.That(result.UserList.Count, Is.EqualTo(4));
            User user1 = result.UserList[0];
            Assert.That(user1.Username, Is.EqualTo("Nicolas"));
            Assert.That(user1.Email, Is.EqualTo("leHeros@gmail.com"));
            Assert.That(user1.Password, Is.EqualTo(HashCodeModel.GetSHA256Hash("FeurFeur")));
            Note user1Note = user1.NoteList[0];
            Tags user1Tag = user1.TagList[0];
            Assert.That(user1Note.Name, Is.EqualTo("Note 0"));
            Assert.That(user1Note.LogoPath, Is.EqualTo("DefaultLogo.png"));
            Assert.That(user1Tag.Name, Is.EqualTo("Tag 0"));
            Assert.That(user1Tag.Color, Is.EqualTo("#5555FF"));
        }

        [Test]
        public void LoadDefaultTheme_Test()
        {
            result = manager.GetOnlyDatabaseDefaultTheme();

            Assert.NotNull(result);
            Assert.That(result.ThemeList.Count, Is.EqualTo(2));
            Theme theme1 = result.ThemeList[0];
            Assert.That(theme1.Name, Is.EqualTo("blacktheme"));
            Assert.That(theme1.ColorList[0], Is.EqualTo("#000000"));
            Assert.That(theme1.ColorList[1], Is.EqualTo("#FF00FF"));
            Assert.That(theme1.ColorList[2], Is.EqualTo("#OOFFOO"));
        }

        [Test]
        public void LoadDefaultLogo_Test()
        {
            result = manager.GetOnlyDatabaseDefaultLogo();

            Assert.NotNull(result);
            Assert.That(result.DefaultLogoList.Count, Is.EqualTo(3));
            Logo logo1 = result.DefaultLogoList[0];
            Assert.That(logo1.Name, Is.EqualTo("default"));
            Assert.That(logo1.LogoLink, Is.EqualTo("DefaultLogo.png"));
        }

    }
}
