﻿using Biblioteque_de_Class;
using Notus_Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests_Persistance
{
    public class ToXML_Tests
    {
        PersistenceManager manager;
        Database result;

        [SetUp]
        public void Setup()
        {
            manager = new PersistenceManager(new Stub());
            result = new Database();
        }

        [Test]
        public void SaveDatabaseData_Test()
        {
            PersistenceManager manager2 = new PersistenceManager(new ToXML());
            Database result2 = manager.LoadDatabaseData();

            manager2.SaveDatabaseData(result2);

            Database result3 = manager2.GetOnlyDatabaseUser();
            Assert.NotNull(result3);

            Assert.That(result2.UserList.Count, Is.EqualTo(result3.UserList.Count));
            Assert.That(result2.UserList[0].Username, Is.EqualTo(result3.UserList[0].Username));
            Assert.That(result2.UserList[0].Email, Is.EqualTo(result3.UserList[0].Email));
            Assert.That(result2.UserList[0].Password, Is.EqualTo(result3.UserList[0].Password));
        }

        [Test]
        public void LoadDefaultData_Test()
        {
            PersistenceManager manager2 = new PersistenceManager(new ToXML());
            Database result2 = new();
            result2.SetDefaultThemeList(manager.GetOnlyDatabaseDefaultTheme().ThemeList);
            result2.SetDefaultLogoList(manager.GetOnlyDatabaseDefaultLogo().DefaultLogoList);

            manager2.SaveDefaultData(result2);

            Database result3 = new();
            result3.SetDefaultThemeList(manager2.GetOnlyDatabaseDefaultTheme().ThemeList);
            result3.SetDefaultLogoList(manager2.GetOnlyDatabaseDefaultLogo().DefaultLogoList);
            Assert.NotNull(result3);

            Assert.That(result2.ThemeList.Count, Is.EqualTo(result3.ThemeList.Count));
            Assert.That(result2.DefaultLogoList.Count, Is.EqualTo(result3.DefaultLogoList.Count));

            Assert.That(result2.ThemeList[0].Name, Is.EqualTo(result3.ThemeList[0].Name));
            Assert.That(result2.ThemeList[0].ColorList[0], Is.EqualTo(result3.ThemeList[0].ColorList[0]));
            Assert.That(result2.ThemeList[0].ColorList[1], Is.EqualTo(result3.ThemeList[0].ColorList[1]));
            Assert.That(result2.ThemeList[0].ColorList[2], Is.EqualTo(result3.ThemeList[0].ColorList[2]));

            Assert.That(result2.DefaultLogoList[0].Name, Is.EqualTo(result3.DefaultLogoList[0].Name));
            Assert.That(result2.DefaultLogoList[0].LogoLink, Is.EqualTo(result3.DefaultLogoList[0].LogoLink));

        }

        [Test]
        public void LoadDatabaseData_Test()
        {
            result = manager.GetOnlyDatabaseUser();

            Assert.NotNull(result);
            Assert.NotNull(result.UserList);
            Assert.That(result.UserList.Count, Is.EqualTo(4));
            User user1 = result.UserList[0];
            Assert.That(user1.Username, Is.EqualTo("Nicolas"));
            Assert.That(user1.Email, Is.EqualTo("leHeros@gmail.com"));
            Assert.That(user1.Password, Is.EqualTo(HashCodeModel.GetSHA256Hash("FeurFeur")));
            Note user1Note = user1.NoteList[0];
            Tags user1Tag = user1.TagList[0];
            Assert.That(user1Note.Name, Is.EqualTo("Note 0"));
            Assert.That(user1Note.LogoPath, Is.EqualTo("DefaultLogo.png"));
            Assert.That(user1Tag.Name, Is.EqualTo("Tag 0"));
            Assert.That(user1Tag.Color, Is.EqualTo("#5555FF"));
        }

        [Test]
        public void LoadDefaultTheme_Test()
        {
            result = manager.GetOnlyDatabaseDefaultTheme();

            Assert.NotNull(result);
            Assert.That(result.ThemeList.Count, Is.EqualTo(2));
            Theme theme1 = result.ThemeList[0];
            Assert.That(theme1.Name, Is.EqualTo("blacktheme"));
            Assert.That(theme1.ColorList[0], Is.EqualTo("#000000"));
            Assert.That(theme1.ColorList[1], Is.EqualTo("#FF00FF"));
            Assert.That(theme1.ColorList[2], Is.EqualTo("#OOFFOO"));
        }

        [Test]
        public void LoadDefaultLogo_Test()
        {
            result = manager.GetOnlyDatabaseDefaultLogo();

            Assert.NotNull(result);
            Assert.That(result.DefaultLogoList.Count, Is.EqualTo(3));
            Logo logo1 = result.DefaultLogoList[0];
            Assert.That(logo1.Name, Is.EqualTo("default"));
            Assert.That(logo1.LogoLink, Is.EqualTo("DefaultLogo.png"));
        }
    }
}

